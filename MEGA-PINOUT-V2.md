# New Mega pinout

## Requirements

### Digital input pins

These should be grouped in to pin banks so they can be easily read.

| Pin description | AVR pin | Arduino pin |
| stagePin        | PC0     | 37
| stageLockPin    | PC1     | 36
| abortPin        | PC2     | 35
| mecoPin         | PC3     | 34
| ftpPin          | PC4     | 33
| controlGroup1   | PC5     | 32
| controlGroup2   | PC6     | 31
| controlGroup3   | PC7     | 30
| controlGroup4   | PB0     | 53
| controlGroup5   | PB1     | 52
| controlGroup6   | PB2     | 51
| controlGroup7   | PB3     | 50
| controlGroup8   | PB4     | 10
| controlGroup9   | PB5     | 11
| controlGroup0   | PB6     | 12
| sasInvPin       | PL0     | 49
| rotTransPin     | PL1     | 48
| transUpPin      | PL2     | 47
| transDownPin    | PL3     | 46
| transLeftPin    | PL4     | 45
| transRightPin   | PL5     | 44
| rcsPin          | PL6     | 43
| sasPin          | PL7     | 42
| transForePin    | PA0     | 22
| transAftPin     | PA1     | 23
| gearPin         | PA2     | 24
| brakePin        | PA3     | 25
| brakeInvPin     | PA4     | 26
| lightsPin       | PA5     | 27
| demoPin         | PA6     | 28
| onPin           | PA7     | 29

### Digital out pins

Again, put these all in the same bank.

| Pin description | AVR pin | Arduino pin |
| throttleDownPin | PH0     | 17
| throttleUpPin   | PH1     | 16
| throttleEnPin   | PH3     |  6

### Analogue in

Try again to keep these on a dedicated bank

| Pin description | AVR pin | Arduino pin |
| throttlePin     | PK0     | A8
| axisPin         | PK1     | A9

### PWM out

Again, should these be on their own bank?

**NOTE:** PE bank is shared with serial.

| Pin description | AVR pin | Arduino pin |
| radarAltPin     | PE3     | 5
| vertSpeedPin    | PE4     | 2

### TWI

**NOTE:** TWI connectors should also have a GND pin.

| Pin description | AVR pin | Arduino pin |
| SCL             | PD0     | 21
| SDA             | PD1     | 20
