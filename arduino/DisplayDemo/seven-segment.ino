/* The LEDs on the displays are set up as a matrix.
   x is always the id of the display:
   x=0 - bottom right
   x=1 - bottom left
   x=2 - top right
   x=3 - top left

   y goes from 0 to 7:
   y=7 - last (rightmost) button
   y=6 - K exponent
   y=5 - M exponent
   y=4 - G exponent
   y=3 - -ve light
   y=2 - first button (leftmost)
   y=1 - second button
   y=0 - third button
*/

int demonumcounter = 0;
void seven_segment_init() {
  for (int i=0; i<displays.getDeviceCount(); i++) {
    displays.shutdown(i, false);
    displays.clearDisplay(i);
    displays.setIntensity(i, 3);
  }
}

void update_display_4() {
  // Update the buttons
  byte row = B00000000;
  // This is all different to how it should be. Eh, demo.
  switch (display_status[1]) {
  case MUX_APO:
    row |=1 << 2;
    break;
  case MUX_SURFV:
    row |=1 << 1;
    // do things for surface velocity button
    break;
  case MUX_MONO:
    row |=1 << 0;
    break;
  case MUX_G:
    row |=1 << 7;
    break;
  default:
    row = 0;
    break;
  }

  // illuminate the appropriate button
  displays.setRow(2, 1, row);
}

/* From left to right, bit numbers in a row look as follows:
    1
   6 2
    7
   5 3
    4 0

  With that in mind, here's a handy list of letters:
  A = B01110111
  b = B00011111
  B = B01111111
  c = B00001101
  C = B01001110
  d = B00111101
  E = B01001111
  F = B01000111
  G = B01011110
  h = B00010111
  H = B00110111
  i = B00010000
  I = B00110000
  J = B00111100
  L = B00001110
  l = B00000110 (reversed 1, work with me here)
  n = B00010101
  o = B00011101
  O = B01111110
  P = B01100111
  r = B00000101
  S = B01011011 (indistinguishable from 5)
  t = B00001111
  u = B00011100
  U = B00111110
  y = B00111011
  Z = B01101101 (indistinguishable from 2)
*/

const byte numMessages = 11;
const byte messages[numMessages][8] = {
  { B00011101, B00011100, B10001111, B00110000,
    B00011100, B00010101, B00001101, B00010111 }, // out.lunch
  { B01111111, B01001111, B01001111, B01100111,
    B01111111, B01001111, B01001111, B01100111 }, // BEEP BEEP
  { B01100111, B01001111, B00001111, B01001111,
    B00110111, B00000101, B00111101, B00111011 }, // PEtE Hrdy
  { B01000111, B00000101, B01001111, B01001111,
    B00110111, B00111110, B01011110, B01011011 }, // FREE HUGS
  { B00001110, B01110111, B00011100, B00010101,
    B00001101, B10010111, B00010000, B00001111 }, // LAunch.it
  { B00111100, B01001111, B11111111, B00000101,
    B00111110, B00001110, B01001111, B01011011 }, // JEB.rULES
  { B01111111, B00110000, B00001110, B00001110,
    B01111111, B01001111, B01011011, B00001111 }, // BILL BESt
  { B01111111, B01111110, B11111111, B00000101,
    B00111110, B00001110, B01001111, B01011011 }, // BOB.rULES
  { B00010101, B01111110, B01111110, B00011111,
    B01000111, B00011101, B00011101, B00111101 }, // n00b Food
  { B01011011, B00010101, B01110111, B01001110,
    B11011011, B01100111, B00001110, B01011011 }, // SnACS.PLS
  { B01001110, B00000101, B01110111, B01011011,
    B00110111, B00000000, B00010000, B00001111 } // CrASH it
};
const int messageTime = 300;
unsigned long messageUpdate = 0;

const byte message[] = "123456789abcdef0     LCA2018     888888888888888888                   ";
int messageIndex = 0;

void numeric_demo() {
  unsigned long now = millis();
  if (messageUpdate < now) {
    for (int i=0; i<4; i++) {
      displays.setChar(1, i, message[(messageIndex+i)%sizeof(message)], false);
    }
    messageIndex = (messageIndex+1)%sizeof(message);
    messageUpdate = now+messageTime;
  }
}

void numeric_off() {
  displays.clearDisplay(0);
  displays.clearDisplay(1);
  displays.clearDisplay(2);
}

void numeric_display() {
  update_display_4();
  numeric_demo();
}
