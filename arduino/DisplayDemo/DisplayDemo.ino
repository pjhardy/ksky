/* DisplayDemo.ino
   A custom demo mode, with one display plugged in to port 3,
   for LCA 2018.
   Peter Hardy <peter@hardy.dropbear.id.au>
*/

#include <LedControl.h>

const int lcDin = 14;
const int lcClk = 16;
const int lcLoad = 10;
const int numMax = 3;

LedControl displays = LedControl (lcDin, lcClk, lcLoad, numMax);

// We are only using one display, so hard code our digits
const int numAddr = 1;
const int maxDigits[] = { 0, 1, 2, 3 };

/* The LEDs on the displays are set up as a matrix.
   x is always the id of the display:
   x=0 - bottom right
   x=1 - bottom left
   x=2 - top right
   x=3 - top left

   y goes from 0 to 7:
   y=7 - last(rightmost) button
   y=6 - K exponent
   y=5 - M exponent
   y=4 - G exponent
   y=3 - -ve light
   y=2 - first button (leftmost)
   y=1 - second button
   y=0 - third button
*/
const int ledAddr = 2;
const int ledRow = 3;

// Panel 4, top left
const byte MUX_APO = 4; // The mux channel for the apoapsis button
const byte MUX_SURFV = 5; // The mux channel for the surface v button
const byte MUX_MONO = 6; // The mux channel for the mono button
const byte MUX_G = 7; // The mux channel for the G button

// Variables for button handling
uint16_t button_state = 0; // Final debounced button state
uint16_t last_button_state = 0; // The last debounced button state

// Variables for display handling
byte display_status[4]; // What each display is currently showing
byte last_display_status[4]; // What each display was showing last update

unsigned long display_time, display_time_old; // Tracking display updates
unsigned long debounce_time, debounce_time_old; // Tracking button polling
const uint8_t debounce_interval = 10; // How often to sample buttons

void setup() {
  Serial.begin(115200);
  seven_segment_init();
  mux_init();
  Serial.println("Kerbal display controller demo mode online.");
}

void loop() {
  button_poll();
  numeric_display();
}
