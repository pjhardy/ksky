// Testing the WS2812 LEDs
#include <FastLED.h>

// FastLED pins
const int fastLedNum = 31; // How many LEDs are attached
const int fastLedPin =  15; // Data out pin to first LED
const int stageStateLed = 0; // The stage state LED
const int throttleAlarmLed = 1; // Alarm LED on throttle panel
const int rcsLed = 2; // The LED on the left hand stick
const int gearLed = 3; // The LED above the landing gear switch
const int brakeLed = 4; // The LED above the brake switches
const int lightsLed = 5; // The LED above the lights switch
const int sasLed = 6; // The LED above the SAS switch
// Bottom left annunciator LED is number 7.
// Numbering then proceeds in vertical columns across to top right
const int solidLed = 10; // The solid fuel annunciator LED
const int altLed = 11; // The altitude alarm annunciator LED
const int oxygenLed = 13; // The o2 annunciator LED
const int liquidLed = 14; // The liquid fuel annunciator LED
const int xenonLed = 18; // The xenon fuel annunciator LED
const int interceptLed = 19; // The intercept annunciator LED
const int escapeLed = 20; // The escape annunciator LED
const int powerLed = 22; // The power annunciator LED
const int gLed = 24; // The G force annunciator LED
const int chutesLed = 25; // The chutes annunciator LED
const int heatLed = 26; // The heat annunciator LED

boolean state = false;
CRGB leds[fastLedNum];

void setup() {
  FastLED.addLeds<WS2812B, fastLedPin>(leds, fastLedNum);
  for (int i=0; i<fastLedNum; i++) {
    leds[i] = CRGB::Black;
  }
  FastLED.show();
}

void loop() {
  /* leds[random(fastLedNum)].setRGB(random(255), random(255), random(255)); */
  /* FastLED.show(); */
  /* delay(500); */
  for (int i=0; i<fastLedNum; i++) {
    if (state) {
      leds[i] = CRGB::White;
    } else {
      leds[i] = CRGB::Black;
    }
  }
  FastLED.show();
  delay(500);
  state = !state;
}
