// Testing the multiplexer

const int mux0 = 4; // mux0 pin
const int mux1 = 5; // mux1 pin
const int mux2 = 6; // mux2 pin
const int mux3 = 7; // mux3 pin
const int muxS = 8; // mux common pin

boolean getMultiplex(byte chan) {
  digitalWrite(mux0, (chan>>0)&1);
  digitalWrite(mux1, (chan>>1)&1);
  digitalWrite(mux2, (chan>>2)&1);
  digitalWrite(mux3, (chan>>3)&1);
  return digitalRead(muxS);
}

void setup() {
  Serial.begin(115200);

  pinMode(mux0, OUTPUT);
  pinMode(mux1, OUTPUT);
  pinMode(mux2, OUTPUT);
  pinMode(mux3, OUTPUT);
  pinMode(muxS, INPUT);

  digitalWrite(mux0, LOW);
  digitalWrite(mux1, HIGH);
  digitalWrite(mux2, HIGH);
  digitalWrite(mux3, HIGH);

  Serial.println("Multiplex test ready");
}

void loop() {
  Serial.println("Polling pins:");
  Serial.println("=============");
  for (int i=1; i<16; i++) {
    if (getMultiplex(i)) {
      Serial.print(i);
      Serial.println(" is high");
    }
  }
  Serial.println("=============");
  Serial.println();
  delay(1000);
}
