/* KerbalDescent
   Arduino sketch for the "descent panel" portion of the controller.
   This panel has two 0-5V panel meters, a couple of WS2812B LEDs,
   and a switch to control how it works.

   Peter Hardy <peter@hardy.dropbear.id.au>
*/
#include <FastLED.h>

#include "data_packets.h"
#include "TWI_slave.h"

//#define DEBUG

// Need 2x PWM pins for the gauges
// Need 1x input for the switch
// Need 1x FastLED output pin

enum MultiplierStates_t {
  MULTIPLIER_1X,
  MULTIPLIER_1000X
};

MultiplierStates_t MultiplierState = MULTIPLIER_1X;

enum DisplayStates_t {
  DISPLAY_OFF = 0,
  DISPLAY_ON = 1,
  DISPLAY_DEMO = 2
};

struct DisplayStatePacket
{
  byte id;
  DisplayStates_t state;
};

VesselData VData;
DisplayStatePacket DPacket;

const char twi_address = 19;
unsigned char twi_buf[TWI_BUFFER_SIZE];

unsigned long nextButton = 0;
const int buttonInterval = 25;

void setup() {
#ifdef DEBUG
  Serial.begin(115200);
  Serial.println("Kerbal descent panel online");
#endif
  twi_init();

  buttonInit();

  displayInit();
}

void loop() {
  unsigned long now = millis();
  twi_packet_receive();
  if (now > nextButton) {
    buttonUpdate();
    nextButton = now + buttonInterval;
  }
  displayUpdate();
}
