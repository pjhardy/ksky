const int vertSpeedPin = 6; // PWM pin for the vertical speed gauge
const int radarAltPin = 5; // PWM pin for the radar altitude

const int fastLedNum = 2; // How many fastLED LEDs are attached
const int fastLedPin = 4; // Data out pin to first LED
CRGB leds[fastLedNum];

void displayInit() {
  pinMode(vertSpeedPin, OUTPUT);
  pinMode(radarAltPin, OUTPUT);

  FastLED.addLeds<WS2812B, fastLedPin>(leds, fastLedNum);
  for (int i=0; i<fastLedNum; i++) {
    leds[i] = CRGB::Black;
  }
  FastLED.show();
}

void displayUpdate() {
  switch(DPacket.state) {
  case DISPLAY_OFF:
    analogWrite(vertSpeedPin, 127);
    analogWrite(radarAltPin, 0);
    leds[0] = CRGB::Black;
    leds[1] = CRGB::Black;
    FastLED.show();
    break;
  case DISPLAY_ON:
    updateVertSpeed();
    updateRadarAlt();
    updateLEDs();
    break;
  case DISPLAY_DEMO:
    updateDemo();
    break;
  }
}

void updateVertSpeed() {
  // Don't forget that this gauge is "upside down".
  // That is, 0V is at the top of the gauge, and 5V at the bottom.
  // 0-63 maps 100-10
  // 64-127 maps 10-0
  // 128-191 maps 0-(-10)
  // 192-255 maps (-10)-(-100)
  // Also, no multiplier here
  int outputLevel = 0;
  if (VData.VVI > 10) {
    int vvi = min((int)(VData.VVI), 100);
    outputLevel = map(vvi, 10, 100, 63, 0);
  } else if (VData.VVI > -10) { // The middle two segments are identical
    int vvi = (int)(VData.VVI * 10);
    outputLevel = map(vvi, -100, 100, 191, 64);
  } else {
    int vvi = max((int)(VData.VVI), -100);
    outputLevel = map(vvi, -100, -10, 255, 191);
  }
  analogWrite(vertSpeedPin, outputLevel);
#ifdef DEBUG
  Serial.print("vert speed mapped to: ");
  Serial.println(outputLevel);
#endif
  // if radar alt > 10km, colour light blue.
  // else, fade from green to red based on outputLevel
  int hue;
  if ((int)VData.RAlt > 10000) {
    hue = 160; // blue
    leds[0] = CHSV(160, 255, 255);
  } else {
    hue = map(outputLevel, 0, 255, 96, 0);
  }
  leds[0] = CHSV(hue, 255, 255);
}

void updateRadarAlt() {
  // 0-85 maps from 0-10
  // 86-170 maps from 10-100
  // 171-255 maps from 100-1000
  float inboundRalt;
  if (MultiplierState == MULTIPLIER_1X) {
    inboundRalt = VData.RAlt;
  } else {
    inboundRalt = VData.RAlt / 1000;
  }

  int outputLevel = 0;
  if (inboundRalt < 10) {
    // Dodgy fixed-point conversion for more speed and accuracy
    int ralt = (int)(inboundRalt * 10);
    outputLevel = map(ralt, 0, 100, 0, 85);
  } else if (inboundRalt < 1000) {
    int ralt = (int)(inboundRalt);
    outputLevel = map(ralt, 10, 100, 86, 170);
  } else {
    int ralt = min((int)(inboundRalt), 1000);
    outputLevel = map(ralt, 100, 1000, 171, 255);
  }
  analogWrite(radarAltPin, outputLevel);
#ifdef DEBUG
  Serial.print("radar alt mapped to: ");
  Serial.println(outputLevel);
#endif
  // fade light from red at 0 through purple to blue
  int hue = map(outputLevel, 0, 255, 255, 171);
  leds[1] = CHSV(hue, 255, 255);
}

void updateLEDs() {
  FastLED.show();
}

void updateDemo() {
  //
}
