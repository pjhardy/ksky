
const int multiplierPin = 8; // Multiplier switch

uint8_t bounceIdx;
uint8_t bounceBuffer[3];

void buttonInit() {
  pinMode(multiplierPin, INPUT);
  bounceIdx = 0;
}

void buttonUpdate() {
  buttonRead();

  uint8_t debounced = 1;
  for (int x=0; x<3; x++) {
    debounced = debounced & bounceBuffer[x];
  }
  switch(debounced) {
  case 0:
    // TODO: Make sure this is right
    MultiplierState = MULTIPLIER_1X;
    // Switch is off
    break;
  case 1:
    MultiplierState = MULTIPLIER_1000X;
    break;
  }
}

inline void buttonRead() {
  uint8_t cur = 0;
  if (digitalRead(multiplierPin)) {
    cur = 1;
  } else {
    cur = 0;
  }

  bounceBuffer[bounceIdx] = cur;
  bounceIdx++;
  if (bounceIdx > 3) bounceIdx = 0;
}
