// Test the multiplexed inputs.

#include <MuxShield.h>

// MuxShield II control pins
const int muxS0 = 2;
const int muxS1 = 4;
const int muxS2 = 6;
const int muxS3 = 7;
const int muxOUTMD = 8;
const int muxIOS1 = 10;
const int muxIOS2 = 11;
const int muxIOS3 = 12;
const int muxIO1 = A0;
const int muxIO2 = A1;
const int muxIO3 = A2;
MuxShield mux(muxS0, muxS1, muxS2, muxS3, muxOUTMD,
              muxIOS1, muxIOS2, muxIOS3,
              muxIO1, muxIO2, muxIO3);

void setup() {
  mux.setMode(1, DIGITAL_IN);
  mux.setMode(2, DIGITAL_IN);

  Serial.begin(115200);
  Serial.println("MuxBoardTest starting...");
}

void loop() {
  char buf[32];
  for(int bank=1; bank<3; bank++) {
    for(int pin=0; pin<16; pin++) {
      if(mux.digitalReadMS(bank, pin)) {
        snprintf(buf, 32, "bank %i pin %i high", bank, pin);
        Serial.println(buf);
      }
    }
  }
}
