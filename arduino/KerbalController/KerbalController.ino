/* KerbalController
   Arduino sketch for a KSP control panel.
   Interfaces with the KSPSerialIO mod. Based on
   zitronen's example code.
   Refer to documentation for requirements and pinouts.

   Peter Hardy <peter@hardy.dropbear.id.au>
*/

#define DIGITALIO_NO_MIX_ANALOGWRITE
#include <digitalIOPerformance.h>
#include <MuxShield.h>

#include "TWI_Master.h"

// Macro to extract packet details
#define details(name) (uint8_t*)&name,sizeof(name)

const int idleTimer=2000; // Timeout when no message received
const int controlRefresh=25; // How often to send a packet

// These variables store last time various actions happened
unsigned long deadtime, deadtimeOld, controlTime, controlTimeOld,
  debounceTime, debounceTimeOld;
unsigned long now;

boolean connected = false; // Whether we're connected or not.

byte caution = 0, warning = 0, id;

struct VesselData {
  byte id;                //1
  float AP;               //2
  float PE;               //3
  float SemiMajorAxis;    //4
  float SemiMinorAxis;    //5
  float VVI;              //6
  float e;                //7
  float inc;              //8
  float G;                //9
  long TAp;               //10
  long TPe;               //11
  float TrueAnomaly;      //12
  float Density;          //13
  long period;            //14
  float RAlt;             //15
  float Alt;              //16
  float Vsurf;            //17
  float Lat;              //18
  float Lon;              //19
  float LiquidFuelTot;    //20
  float LiquidFuel;       //21
  float OxidizerTot;      //22
  float Oxidizer;         //23
  float EChargeTot;       //24
  float ECharge;          //25
  float MonoPropTot;      //26
  float MonoProp;         //27
  float IntakeAirTot;     //28
  float IntakeAir;        //29
  float SolidFuelTot;     //30
  float SolidFuel;        //31
  float XenonGasTot;      //32
  float XenonGas;         //33
  float LiquidFuelTotS;   //34
  float LiquidFuelS;      //35
  float OxidizerTotS;     //36
  float OxidizerS;        //37
  uint32_t MissionTime;   //38
  float deltaTime;        //39
  float VOrbit;           //40
  uint32_t MNTime;        //41
  float MNDeltaV;         //42
  float Pitch;            //43  
  float Roll;             //44  
  float Heading;          //45
  uint16_t ActionGroups;  //46 status bit order:SAS, RCS, Light, Gear, Brakes, Abort, Custom01-10
  byte SOINumber;         //47 SOI Number (decimal format: sun-planet-moon e.g. 130 = kerbin, 131 = mun)
  byte MaxOverHeat;       //48  Max part overheat (% percent)
  float MachNumber;       //49
  float IAS;              //50  Indicated Air Speed
  byte CurrentStage;      //51
  byte TotalStage;        //52
  byte FarSOINumber;      //53 The SOI of the farthest patch. 0 if uninterrupted orbit
  float FarAP;            //54 The AP of the farthest patch.
  float FarPE;            //55 The PE of the farthest patch.
};

struct HandShakePacket {
  byte id;
  byte M1;
  byte M2;
  byte M3;
};

enum DisplayStates_t {
  DISPLAY_OFF = 0,
  DISPLAY_ON = 1,
  DISPLAY_DEMO = 2
};

struct DisplayStatePacket
{
  byte id;
  DisplayStates_t state;
};

HandShakePacket HPacket;
VesselData VData;
DisplayStatePacket DPacket;

// MuxShield pin assignments
// Bank 1 - digital in
const int stagePin[] = {0, 0}; // Stage button
const int stageLockPin[] = {0, 1}; // Stage lock
const int abortPin[] = {0, 2}; // Abort button
const int mecoPin[] = {0, 3}; // Main Engine CutOut switch
const int ftpPin[] = {0, 4}; // Full Throttle Position switch

// Control group buttons 1-10
const int controlPin[10][2] = {{0, 5}, {0, 6}, {0, 7}, {0, 8},
			       {0, 9}, {0, 10}, {0, 11},
			       {0, 12}, {0, 13}, {0, 14}};

// Bank 2 - digital in
const int SasInvPin[] = {1, 0};
const int RotTransPin[] = {1, 1};
const int TransUpPin[] = {1, 2};
const int TransDownPin[] = {1, 3};
const int TransLeftPin[] = {1, 4};
const int TransRightPin[] = {1, 5};
const int RcsPin[] = {1, 6};
const int SasPin[] = {1, 7};
const int TransForePin[] = {1, 8};
const int TransAftPin[] = {1, 9};
const int GearPin[] = {1, 11};
const int BrakePin[] = {1, 10};
const int BrakeInvPin[] = {1, 12};
const int LightsPin[] = {1, 13};
const int DemoPin[] = {1, 14};
const int OnPin[] = {1, 15};

// Bank 3 - digital out
const int throttleDownPin[] = {3, 0}; // Motor direction to move throttle down
const int throttleUpPin[] = {3, 1}; // Motor direction to move throttle up

const int throttleEnPin = 5; // PWM pin to control throttle motor speed

// MuxShield II control pins
const int muxS0 = 2;
const int muxS1 = 4;
const int muxS2 = 6;
const int muxS3 = 7;
const int muxOUTMD = 8;
const int muxIOS1 = 10;
const int muxIOS2 = 11;
const int muxIOS3 = 12;
const int muxIO1 = A0;
const int muxIO2 = A1;
const int muxIO3 = A2;
MuxShield mux(muxS0, muxS1, muxS2, muxS3, muxOUTMD,
              muxIOS1, muxIOS2, muxIOS3,
              muxIO1, muxIO2, muxIO3);

// Button states
uint16_t muxState[2] = {0}; // Final debounced state
uint16_t lastMuxState[2] = {0}; // The last debounced state
// We track the control group state, latched to the leading
// edge of button state changes.
uint16_t controlGroupToggle[2] = {0};

long tick = 0; // Current time
const uint8_t debounceInterval = 10; // How often to sample buttons
long lastButtonRead = 0; // Last time we debounced the buttons
long thisButtonRead = 0;

const uint8_t sendInterval = 50; // How often to send serial state
long lastSerialSend = 0; // Last time we sent serial data
bool twi_vdata_packet = false; // True if VData needs to be broadcast over I2C
bool twi_dpacket_packet = false; // True if DPacket needs to be broadcast over I2C

// Throttle variables
const int throttlePin = A3; // Which pin the throttle is attached to
long throttleLevel = 0; // Adjusted throttle; 0 - 1000
enum throttleStates {
  NORMAL,
  FTP,
  MECO
};
throttleStates throttleState = NORMAL;

const int axisPin = A4; // Which pin the adj pot is attached to
long axisAmount = 0; // Value of adj pot - what do send for control axes

// State variables
bool abortState = false; // True when abort button has been pressed

void setup(){
  Serial.begin(38400);

  InitTxPackets();

  twi_init();

  mux.setMode(1, DIGITAL_IN);
  mux.setMode(2, DIGITAL_IN);
  mux.setMode(3, DIGITAL_OUT);

  pinMode(throttleEnPin, OUTPUT);
  analogWrite(throttleEnPin, 0);

  // Make sure the throttle is zeroed when switching to new vessel.
  throttleState = MECO;
}

void loop()
{
  debounce();
  input();
  twi_send();
  output();
}
