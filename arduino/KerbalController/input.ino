/* KerbalController
   Arduino sketch for a KSP control panel.
   Interfaces with the KSPSerialIO mod. Based on
   zitronen's example code.
   Refer to documentation for requirements and pinouts.

   Peter Hardy <peter@hardy.dropbear.id.au>
*/

void input() {
  now = millis();

  if (KSPBoardReceiveData()){
    deadtimeOld = now;
    switch(id) {
    case 0: //Handshake packet
      handshake();
      break;
    case 1:
      twi_vdata_packet = true; // blat packet on to TWI bus
      break;
    }
    connected = true;
  }
  else
  { //if no message received for a while, go idle
    deadtime = now - deadtimeOld; 
    if (deadtime > idleTimer)
    {
      deadtimeOld = now;
      connected = false;
    }    
  }
}

byte ControlStatus(byte n) {
  return ((VData.ActionGroups >> n) & 1) == 1;
}
