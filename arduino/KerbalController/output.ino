/* KerbalController
   Arduino sketch for a KSP control panel.
   Interfaces with the KSPSerialIO mod. Based on
   zitronen's example code.
   Refer to documentation for requirements and pinouts.

   Peter Hardy <peter@hardy.dropbear.id.au>
*/

// MainControls bits
const int SAS=7;
const int RCS=6;
const int LIGHTS=5;
const int GEAR=4;
const int BRAKES=3;
const int PRECISION=2;
const int ABORT=1;
const int STAGE=0;

struct ControlPacket {
  byte id;
  byte MainControls;                  //SAS RCS Lights Gear Brakes Precision Abort Stage 
  byte Mode;                          //0 = stage, 1 = docking, 2 = map
  unsigned int ControlGroup;          //control groups 1-10 in 2 bytes
  byte AdditionalControlByte1;        //other stuff
  byte AdditionalControlByte2;
  int Pitch;                          //-1000 -> 1000
  int Roll;                           //-1000 -> 1000
  int Yaw;                            //-1000 -> 1000
  int TX;                             //-1000 -> 1000
  int TY;                             //-1000 -> 1000
  int TZ;                             //-1000 -> 1000
  int WheelSteer;                     //-1000 -> 1000
  int Throttle;                       //    0 -> 1000
  int WheelThrottle;                  //    0 -> 1000
};

ControlPacket CPacket;

void InitTxPackets() {
  HPacket.id = 0;
  CPacket.id = 101;
  DPacket.id = 2;
}

void output() {
  now = millis();
  controlTime = now - controlTimeOld; 
  if (controlTime > controlRefresh){
    controlTimeOld = now;
    controls();
  }    
}

boolean stageStatus() {
  if (readMuxPin(stagePin) && readMuxPin(stageLockPin)) {
    return true;
  } else {
    return false;
  }
}

boolean abortStatus() {
  return readMuxPin(abortPin);
}

boolean sasStatus() {
  boolean SasPinStatus = readMuxPin(SasPin);
  if (readMuxPin(SasInvPin)) {
    SasPinStatus = !SasPinStatus;
  }
  return SasPinStatus;
}

boolean rcsStatus() {
  return (readMuxPin(RcsPin));
}

boolean gearStatus() {
  return readMuxPin(GearPin);
}

boolean brakeStatus() {
  boolean BrakePinStatus = readMuxPin(BrakePin);
  if (readMuxPin(BrakeInvPin)) {
    BrakePinStatus = !BrakePinStatus;
  }
  return BrakePinStatus;
}

boolean lightStatus() {
  return (readMuxPin(LightsPin));
}

void controls() {
  if (connected) {
    if (stageStatus()) {
      MainControls(STAGE, HIGH);
    } else {
      MainControls(STAGE, LOW);
    }
    if (abortStatus()) {
      abortState = true;
      MainControls(ABORT, HIGH);
    } else {
      MainControls(ABORT, LOW);
    }
    if (sasStatus()) {
      MainControls(SAS, HIGH);
    } else {
      MainControls(SAS, LOW);
    }
    if (rcsStatus()) {
      MainControls(RCS, HIGH);
    } else {
      MainControls(RCS, LOW);
    }
    if (gearStatus()) {
      MainControls(GEAR, HIGH);
    } else {
      MainControls(GEAR, LOW);
    }
    if (brakeStatus()) {
      MainControls(BRAKES, HIGH);
    } else {
      MainControls(BRAKES, LOW);
    }
    if (lightStatus()) {
      MainControls(LIGHTS, HIGH);
    } else {
      MainControls(LIGHTS, LOW);
    }

    // Popuplate control group bytes
    for (int i=0; i<10; i++) {
      if (controlGroupToggle[0] & ((uint16_t)1 << (i+4))) {
	ControlGroups(i, HIGH);
      } else {
	ControlGroups(i, LOW);
      }
    }
    CPacket.Throttle = throttleLevel;

    if (readMuxPin(TransUpPin)) {
      if (readMuxPin(RotTransPin)) {
        CPacket.Pitch = 0-axisAmount; // Up == pitch forward, troglodytes.
        CPacket.TY = 0;
      } else {
        CPacket.TY = axisAmount; // Translate up
        CPacket.Pitch = 0;
      }
    } else if (readMuxPin(TransDownPin)) {
      if (readMuxPin(RotTransPin)) {
        CPacket.Pitch = axisAmount;
        CPacket.TY = 0;
      } else {
        CPacket.TY = 0-axisAmount; // Translate down
        CPacket.Pitch = 0;
      }
    } else {
      CPacket.TY = 0;
      CPacket.Pitch = 0;
    }
    if (readMuxPin(TransLeftPin)) {
      if (readMuxPin(RotTransPin)) {
        CPacket.Yaw = 0-axisAmount;
        CPacket.TX = 0;
      } else {
        CPacket.TX = axisAmount;
        CPacket.Yaw = 0;
      }
      CPacket.WheelSteer = axisAmount;
    } else if (readMuxPin(TransRightPin)) {
      if (readMuxPin(RotTransPin)) {
        CPacket.Yaw = axisAmount;
        CPacket.TX = 0;
      } else {
        CPacket.TX = 0-axisAmount;
        CPacket.Yaw = 0;
      }
      CPacket.WheelSteer = 0-axisAmount;
    } else {
      CPacket.TX = 0;
      CPacket.Yaw = 0;
      CPacket.WheelSteer = 0;
    }
    if (readMuxPin(TransForePin)) {
      // This stick currently does nothing in rot mode.
      if (!readMuxPin(RotTransPin)) {
        CPacket.TZ = 0-axisAmount; // http://wiki.kerbalspaceprogram.com/wiki/API:FlightCtrlState says -1 is forward
      }
      // Rovers only know two speeds: All or none. :-)
      CPacket.WheelThrottle = 1000;
    } else if (readMuxPin(TransAftPin)) {
      if (!readMuxPin(RotTransPin)) {
        CPacket.TZ = axisAmount;
      }
      CPacket.WheelThrottle = -1000;
    } else {
      CPacket.TZ = 0;
      CPacket.WheelThrottle = 0;
    }

    KSPBoardSendData(details(CPacket));
  }
}

void MainControls(byte n, boolean s) {
  if (s)
    CPacket.MainControls |= (1 << n);       // forces nth bit of x to be 1.  all other bits left alone.
  else
    CPacket.MainControls &= ~(1 << n);      // forces nth bit of x to be 0.  all other bits left alone.
}

void ControlGroups(byte n, boolean s) {
  if (s)
    CPacket.ControlGroup |= (1 << n);       // forces nth bit of x to be 1.  all other bits left alone.
  else
    CPacket.ControlGroup &= ~(1 << n);      // forces nth bit of x to be 0.  all other bits left alone.
}
