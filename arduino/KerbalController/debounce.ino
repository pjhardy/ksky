/* KerbalController
   Arduino sketch for a KSP control panel.
   Interfaces with the KSPSerialIO mod. Based on
   zitronen's example code.
   Refer to documentation for requirements and pinouts.

   Peter Hardy <peter@hardy.dropbear.id.au>
*/

const int throttleDeadzone = 10; // deadzones at top and bottom of throttle

void throttleControl() {
  // analogRead returns 0-1024.
  // Readings between the lower and upper deadzone should be mapped to
  // the throttle param scale of 0-1000.
  // Also, my throttle is upside down - full throttle position is 0 and
  // meco is 1024. Hence flipping highs and lows.
  long curThrottle = analogRead(throttlePin); // raw throttle
  // This should treat dead zones as completely invisible.
  curThrottle = max(curThrottle-(2*throttleDeadzone), 0);
  curThrottle = constrain(map(analogRead(throttlePin),
                              1024-(2*throttleDeadzone), 0,
                              0, 1000), 0, 1000);
  if (readMuxPin(mecoPin)) {
    throttleState = MECO;
  } else if (readMuxPin(ftpPin)) {
    throttleState = FTP;
  }

  switch (throttleState) {
  case NORMAL:
    throttleLevel = curThrottle;
    break;
  case FTP:
    throttleLevel = 1000;
    if (curThrottle == throttleLevel) {
      throttleState = NORMAL;
      analogWrite(throttleEnPin, 0);
      digitalWriteMuxPin(throttleUpPin, LOW);
    } else {
      digitalWriteMuxPin(throttleUpPin, HIGH);
      analogWrite(throttleEnPin, 220);
    }
    break;
  case MECO:
    throttleLevel = 0;
    if (curThrottle == throttleLevel) {
      throttleState = NORMAL;
      analogWrite(throttleEnPin, 0);
      digitalWriteMuxPin(throttleDownPin, LOW);
    } else {
      digitalWriteMuxPin(throttleDownPin, HIGH);
      analogWrite(throttleEnPin, 220);
    }
    break;
  }
}

void displayMode() {
  DisplayStates_t newState = DISPLAY_OFF;
  if (readMuxPin(OnPin)) {
    newState = DISPLAY_ON;
  } else if (readMuxPin(DemoPin)) {
    newState = DISPLAY_DEMO;
  }
  if (newState != DPacket.state) {
    DPacket.state = newState;
    twi_dpacket_packet = true;
  }
}

void controlGroupLatch() {
  uint16_t mask;
  uint16_t state = controlGroupToggle[0];
  // Only care about the first mux bank at the moment
  mask = ~lastMuxState[0] & muxState[0]; // Mask of all changed inputs
  mask = mask & muxState[0]; // Mask of changed inputs that are now high
  state = state ^ mask; // Apply mask to toggle state
  controlGroupToggle[0] = state;
}

void debounce() {
  now = millis();
  debounceTime = now - debounceTimeOld;
  if (debounceTime > debounceInterval) {
    debounceTimeOld = now;
    for (int i=0; i<2; i++) {
      lastMuxState[i] = muxState[i];
    }

    debounceSwitches();
    throttleControl();
    controlGroupLatch();
    displayMode();
    axisAmount = constrain(map(analogRead(axisPin),
                               0, 1024, 0, 1000), 0, 1000);
  }
}
