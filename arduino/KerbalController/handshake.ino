/* KerbalController
   Arduino sketch for a KSP control panel.
   Interfaces with the KSPSerialIO mod. Based on
   zitronen's example code.
   Refer to documentation for requirements and pinouts.

   Peter Hardy <peter@hardy.dropbear.id.au>
*/

void handshake(){
  HPacket.id = 0;
  HPacket.M1 = 3;
  HPacket.M2 = 1;
  HPacket.M3 = 4;

  KSPBoardSendData(details(HPacket));
}
