/* KerbalController
   Arduino sketch for a KSP control panel.
   Interfaces with the KSPSerialIO mod. Based on
   zitronen's example code.
   Refer to documentation for requirements and pinouts.

   Peter Hardy <peter@hardy.dropbear.id.au>
*/

const int debounceChecks = 3; // Checks before a switch is debounced.

uint16_t bank1BounceState[debounceChecks] = {0}; // Bank 1 raw readings
uint16_t bank2BounceState[debounceChecks] = {0}; // Bank 2 raw readings
int bankIndex = 0; // Index in to raw readings list

uint16_t readMuxPin(const int pin[2]) {
  return muxState[pin[0]] & ((uint16_t)1 << pin[1]);
}

void digitalReadMuxes(uint16_t banks[2]) {
  // TODO: Reading 16 sequentially is the worst way I can
  // imagine doing this. But it's also the only way I've
  // made it work so far. :-(
  for (int i=0; i<16; i++) {
    if (mux.digitalReadMS(1, i)) {
      banks[0] |= 1 << i;
    } else {
      banks[0] &= ~(1 << i);
    }
    if (mux.digitalReadMS(2, i)) {
      banks[1] |= 1 << i;
    } else {
      banks[1] &= ~(1 << i);
    }
  }
}

void digitalWriteMuxPin(const int pin[2], int val) {
  int i;
  int bank = pin[0];
  int output = pin[1];
  mux.digitalWriteMS(bank, output, val);
}

// Debouncing courtesy of http://www.embedded.com/electronics-blogs/break-points/4024981/My-favorite-software-debouncers
void debounceSwitches() {
  uint16_t j, k;
  uint16_t banks[2];
  digitalReadMuxes(banks);
  bank1BounceState[bankIndex] = banks[0];
  bank2BounceState[bankIndex] = banks[1];
  ++bankIndex;
  j = 0xffff;
  k = 0xffff;
  for (int i=0; i<debounceChecks; i++) {
    j = j & bank1BounceState[i];
    k = k & bank2BounceState[i];
  }
  muxState[0] = j;
  muxState[1] = k;
  if (bankIndex >= debounceChecks) {
    bankIndex = 0;
  }
}

