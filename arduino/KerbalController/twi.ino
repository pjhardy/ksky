unsigned char twi_buf[TWI_BUFFER_SIZE];
static unsigned char twi_address=0; // 0 is broadcast address

void twi_init() {
  TWI_Master_Initialise();
  interrupts(); // Arduino-ey interrupt enable. I hope this works.

  twi_vdata_packet = false;
}

void twi_send_vdata() {
  if (!TWI_Transceiver_Busy()) {
    uint8_t packetlength = min(sizeof(VData),255);

    twi_buf[0] = (twi_address<<TWI_ADR_BITS) | (FALSE<<TWI_READ_BIT); // The first byte contains TWI slave address and read bit
    twi_buf[1] = packetlength;
    memcpy(&twi_buf[2], &VData, packetlength);

    // Computing checksum
    uint8_t cs = packetlength;
    for (int i=0; i<packetlength; i++) {
      cs ^= twi_buf[2+i];
    }
    twi_buf[packetlength+2] = cs;
    TWI_Start_Transceiver_With_Data(twi_buf, packetlength+3);
    twi_vdata_packet = false;
  }
}

void twi_send_dpacket() {
  if (!TWI_Transceiver_Busy()) {
    uint8_t packetlength = min(sizeof(DPacket), 255);

    twi_buf[0] = (twi_address<<TWI_ADR_BITS) | (FALSE<<TWI_READ_BIT); // The first byte contains TWI slave address and read bit
    twi_buf[1] = packetlength;
    memcpy(&twi_buf[2], &DPacket, packetlength);

    // Computing checksum
    uint8_t cs = packetlength;
    for (int i=0; i<packetlength; i++) {
      cs ^= twi_buf[2+i];
    }
    twi_buf[packetlength+2] = cs;
    TWI_Start_Transceiver_With_Data(twi_buf, packetlength+3);
    twi_dpacket_packet = false;
  }
}

void twi_send() {
  if (twi_vdata_packet) {
    twi_send_vdata();
  }
  if (twi_dpacket_packet) {
    twi_send_dpacket();
  }
}
