// Testing the MuxShield inputs on the Mega
#include <MuxShield.h>

// MuxShield II control pins
const int muxS0 = 2;
const int muxS1 = 4;
const int muxS2 = 6;
const int muxS3 = 7;
const int muxOUTMD = 8;
const int muxIOS1 = 10;
const int muxIOS2 = 11;
const int muxIOS3 = 12;
const int muxIO1 = A0;
const int muxIO2 = A1;
const int muxIO3 = A2;
MuxShield mux(muxS0, muxS1, muxS2, muxS3, muxOUTMD,
              muxIOS1, muxIOS2, muxIOS3,
              muxIO1, muxIO2, muxIO3);

void setup() {
  delay(5000);
  mux.setMode(1, DIGITAL_IN);
  mux.setMode(2, DIGITAL_IN);
  mux.setMode(3, DIGITAL_OUT);

  Serial.begin(115200);
  Serial.println("Mux Shield test ready:");
}

void loop() {
  for (int x=0; x<16; x++) {
    if (mux.digitalReadMS(1, x)) {
      Serial.print("bank 1 pin ");
      Serial.print(x);
      Serial.println(" is high.");
    }
    if (mux.digitalReadMS(2, x)) {
      Serial.print("bank 2 pin ");
      Serial.print(x);
      Serial.println(" is high.");
    }
  }
}
