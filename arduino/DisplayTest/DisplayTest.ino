/* Testing the display fu. */

#include <LedControl.h>

const int lcDin = 14; // updated
const int lcClk = 16; // updated
const int lcLoad = 10;
const int numMax = 3;

LedControl maxLeds=LedControl(lcDin, lcClk, lcLoad, numMax);

long interval = 100;

boolean state = false;
void setup() {
  Serial.begin(115200);
  /* pinMode(lcDin, OUTPUT); */
  /* pinMode(lcClk, OUTPUT); */
  /* pinMode(lcLoad, OUTPUT); */
  /* digitalWrite(lcDin, HIGH); */

  for (int i=0; i<maxLeds.getDeviceCount(); i++) {
    maxLeds.shutdown(i, false);
    maxLeds.setIntensity(i, 3);
  }
  
    // 12.00
  for (int i=0; i<2; i++) {
    maxLeds.setChar(i, 0, 1, false);
    maxLeds.setChar(i, 4, 1, false);
    maxLeds.setChar(i, 1, 2, true);
    maxLeds.setChar(i, 5, 2, true);
    maxLeds.setChar(i, 2, 0, false);
    maxLeds.setChar(i, 6, 0, false);
    maxLeds.setChar(i, 3, 0, false);
    maxLeds.setChar(i, 7, 0, false);
  }

  /* for (int x=0; x<8; x++) { */
  /*   for (int y=0; y<8; y++) { */
  /*     maxLeds.setLed(2, x, y, true); */
  /*   } */
  /* } */
}

void loop() {
  /* for (int y=0; y<8; y++) { */
  /*   for (int x=0; x<4; x++) { */
  /*     maxLeds.setLed(2, x, y, true); */
  /*     Serial.print("x: "); */
  /*     Serial.print(x); */
  /*     Serial.print(" y: "); */
  /*     Serial.println(y); */
  /*     delay(1000); */
  /*     maxLeds.setLed(2, x, y, false); */
  /*   } */
  /* } */

  // Strobe through elements of a row
  for (int i=0; i<8; i++) {
    byte row = B00000000;
    //maxLeds.setRow(2, 0, row);
    row |= 1 << i;
    maxLeds.setRow(2, 0, row);
    delay(1000);
  }

  // Blink all in a row on and off
  /* maxLeds.setRow(2, 0, B00000000); */
  /* delay(1000); */
  /* maxLeds.setRow(2, 0, B11111111); */
  /* delay(1000); */
}
