# Makefile targetting the Arduino mega. Symlink to sketch folders that
# contain code intended for it.

# Target board. See https://github.com/arduino/Arduino/blob/ide-1.5.x/build/shared/manpage.adoc
# for how to specify this
BOARD=arduino:avr:uno

# This emulates the Arduino IDE's requirement for
# putting a project in a folder of the same name.
INO=$(notdir $(CURDIR)).ino

# Serial port the board is connected to
PORT=/dev/tty.usbmodem1421

ARDUINOCMD=arduino -v --board $(BOARD) --port $(PORT)

all:
	$(ARDUINOCMD) --verify $(PWD)/$(INO)

install:
	$(ARDUINOCMD) --upload $(PWD)/$(INO)
