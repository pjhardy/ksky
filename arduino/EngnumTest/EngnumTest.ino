#include "engnumber.h"

char buf[16];

void setup() {
  Serial.begin(115200);
  Serial.println("Test starting");
}

void loop() {
  EngNumber test;

  float testnums[] = {1200.0, 40311.123, 41234.0, 123456.7, 1234567.890};
  for (int i=0; i<5; i++) {
    float_to_eng(testnums[i], &test);

    Serial.print("Converted ");
    Serial.print(testnums[i]);
    Serial.print(" to ");
    for (int i=0; i<4; i++) {
      Serial.print(test.digits[i]);
    }
    Serial.print(" dp=");
    Serial.print(test.dp);
    Serial.print(" exp=");
    Serial.println(test.exponent);
    Serial.println();
  }
  delay(500);
}
