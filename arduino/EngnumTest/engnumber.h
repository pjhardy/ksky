#include <Arduino.h>

#define ENGNUM_DIGITS 4

#define FLOAT_MAX_DIGITS 7

struct EngNumber {
  bool negative;
  int digits[ENGNUM_DIGITS];
  int dp;
  int exponent;
};

void float_to_eng(double number, EngNumber *engnum);
