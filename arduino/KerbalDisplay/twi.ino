const byte vdata_id = 1; // ID of VData packet
const byte dpacket_id = 2; // ID of DPacket packet

void twi_init() {
#ifdef DEBUG
  Serial.println("Initialising TWI");
#endif
  TWI_Slave_Initialise((unsigned char)((twi_address<<TWI_ADR_BITS) | (TRUE<<TWI_GEN_BIT)));
  interrupts();
  // Wait until transceiver not busy, then start it
  while (TWI_Transceiver_Busy());
  TWI_Start_Transceiver();
#ifdef DEBUG
  Serial.println("Done");
#endif
}

void twi_packet_receive() {
  // Check if the TWI transceiver has completed an operation
  if (!TWI_Transceiver_Busy()) {
    // Check if the last operation was successful
    if (TWI_statusReg.lastTransOK) {
      // Check if the last operation was a reception
      if (TWI_statusReg.RxDataInBuf) {
        TWI_Get_Data_From_Transceiver(twi_buf, sizeof(VData)+2);
        uint8_t packetsize = twi_buf[0];
        // check checksum
        uint8_t cs = packetsize;
        for (int i=0; i<packetsize; i++) {
          cs ^= twi_buf[1+i];
        }
        if (twi_buf[packetsize+1] != cs) {
          // Checksum failed
          return;
        }

        unsigned long now;
        switch(twi_buf[1]) {
        case 1:
          memcpy(&VData, &twi_buf[1], sizeof(VData));
          now = millis();
          //update_display(now);
          break;
        case 2:
          memcpy(&DPacket, &twi_buf[1], sizeof(DPacket));
          // I don't think anything else needs to be done here.
          // Blanking and everything else should be handled by
          // the code that handles different display states.
          break;
        default:
          break;
        }
      }
      if (!TWI_Transceiver_Busy()) {
        TWI_Start_Transceiver();
      }
    }
  }
}
