/* KerbalDisplay
   Arduino sketch for the display portion of the control panel.
   Interfaces with KerbalController, receiving packets broadcast
   by it over I2C.

   Peter Hardy <peter@hardy.dropbear.id.au>
*/

#include <LedControl.h>
#include <FastLED.h>

#include "TWI_slave.h"

//#define DEBUG

struct VesselData
{
  byte id;                //1
  float AP;               //2
  float PE;               //3
  float SemiMajorAxis;    //4
  float SemiMinorAxis;    //5
  float VVI;              //6
  float e;                //7
  float inc;              //8
  float G;                //9
  long TAp;               //10
  long TPe;               //11
  float TrueAnomaly;      //12
  float Density;          //13
  long period;            //14
  float RAlt;             //15
  float Alt;              //16
  float Vsurf;            //17
  float Lat;              //18
  float Lon;              //19
  float LiquidFuelTot;    //20
  float LiquidFuel;       //21
  float OxidizerTot;      //22
  float Oxidizer;         //23
  float EChargeTot;       //24
  float ECharge;          //25
  float MonoPropTot;      //26
  float MonoProp;         //27
  float IntakeAirTot;     //28
  float IntakeAir;        //29
  float SolidFuelTot;     //30
  float SolidFuel;        //31
  float XenonGasTot;      //32
  float XenonGas;         //33
  float LiquidFuelTotS;   //34
  float LiquidFuelS;      //35
  float OxidizerTotS;     //36
  float OxidizerS;        //37
  uint32_t MissionTime;   //38
  float deltaTime;        //39
  float VOrbit;           //40
  uint32_t MNTime;        //41
  float MNDeltaV;         //42
  float Pitch;            //43  
  float Roll;             //44  
  float Heading;          //45
  uint16_t ActionGroups;  //46 status bit order:SAS, RCS, Light, Gear, Brakes, Abort, Custom01-10
  byte SOINumber;         //47 SOI Number (decimal format: sun-planet-moon e.g. 130 = kerbin, 131 = mun)
  byte MaxOverHeat;       //48  Max part overheat (% percent)
  float MachNumber;       //49
  float IAS;              //50  Indicated Air Speed
  byte CurrentStage;      //51
  byte TotalStage;        //52
  byte FarSOINumber;      //53 The SOI of the farthest patch. 0 if uninterrupted orbit
  float FarAP;            //54 The AP of the farthest patch.
  float FarPE;            //55 The PE of the farthest patch.
};

enum OrbitStates_t {
  STATE_ORBIT = 0,
  STATE_ESCAPE = 1,
  STATE_INTERCEPT = 2
};

OrbitStates_t orbit_state = STATE_ORBIT;

enum DisplayStates_t {
  DISPLAY_OFF = 0,
  DISPLAY_ON = 1,
  DISPLAY_DEMO = 2
};

struct DisplayStatePacket
{
  byte id;
  DisplayStates_t state;
};

VesselData VData;
DisplayStatePacket DPacket;

const char twi_address = 17;
unsigned char twi_buf[TWI_BUFFER_SIZE];

LedControl displays = LedControl(14, 16, 10, 3);

// FastLED pins
const int fastLedNum = 31; // How many LEDs are attached
const int fastLedPin =  15; // Data out pin to first LED

//const int gearLed = 0; // The LED above the landing gear switch
//const int brakeLed = 1; // The LED above the brake switches
//const int lightsLed = 2; // The LED above the light switch
const int throttleAlarmLed = 0; // Alarm LED on throttle panel
const int rcsLed = 1; // The LED on the left hand stick
const int stageStateLed = 2; // The stage state LED
const int sasLed = 3; // The LED above the SAS switch
// Bottom left annunciator LED is number 4.
// Numbering then proceeds in vertical columns across to top right
const int solidLed = 7; // The solid fuel annunciator LED
const int altLed = 8; // The altitude alarm annunciator LED
const int oxygenLed = 10; // The o2 annunciator LED
const int liquidLed = 11; // The liquid fuel annunciator LED
const int xenonLed = 15; // The xenon fuel annunciator LED
const int interceptLed = 16; // The intercept annunciator LED
const int escapeLed = 17; // The escape annunciator LED
const int powerLed = 19; // The power annunciator LED
const int gLed = 21; // The G force annunciator LED
const int chutesLed = 22; // The chutes annunciator LED
const int heatLed = 23; // The heat annunciator LED

// FastLED initialisation
CRGB leds[fastLedNum];

// Panel 1, bottom right
const byte MUX_PET = 0; // The mux channel for the time to PE button
const byte MUX_NODEDV = 2; // The mux channel for the node dV button
const byte MUX_OXS = 1; // The mux channel for the O2 stage button
const byte MUX_RALT = 3; // The mux channel for the radar alt button
// Panel 2, bottom left
const byte MUX_PERI = 4; // The mux channel for the periapsis button
const byte MUX_NODET = 5; // The mux channel for the time to node button
const byte MUX_LFS = 6; // The mux channel for the stage LF button
const byte MUX_ALT = 7; // The mux channel for the alt button
// Panel 3, top right
const byte MUX_APT = 8; // The mux channel for the time to AP button
const byte MUX_VERTV = 9; // The mux channel for the vertical velocity
const byte MUX_ELECTRIC = 10; // The mux channel for electric charge
const byte MUX_INC = 11; // The mux channel for inclination button
// Panel 4, top left
const byte MUX_APO = 12; // The mux channel for the apoapsis button
const byte MUX_SURFV = 13; // The mux channel for the surface v button
const byte MUX_MONO = 14; // The mux channel for the mono button
const byte MUX_G = 15; // The mux channel for the G button

// Variables for button handling
uint16_t button_state = 0; // Final debounced button state
uint16_t last_button_state = 0; // The last debounced button state

// Variables for display handling
byte display_status[4]; // What each display is currently showing
byte last_display_status[4]; // What each display was showing last update

unsigned long display_time, display_time_old; // Tracking display updates
unsigned long debounce_time, debounce_time_old; // Tracking button polling
const uint8_t debounce_interval = 10; // How often to sample buttons

void setup() {
#ifdef DEBUG
  Serial.begin(115200);
  Serial.println("Kerbal display controller online.");
#endif
  twi_init();
  TWI_Start_Transceiver();

  FastLED.addLeds<WS2812B, fastLedPin>(leds, fastLedNum);
  for (int i=0; i<fastLedNum; i++) {
    leds[i] = CRGB::Black;
  }
  FastLED.show();

  seven_segment_init();

  mux_init();

  // TODO: Handle this properly
  DPacket.state = DISPLAY_OFF;
}

void loop() {
  twi_packet_receive();
  button_poll();
  display();
}
