const int mux0 = 4; // mux0 pin
const int mux1 = 5; // mux1 pin
const int mux2 = 6; // mux2 pin
const int mux3 = 7; // mux3 pin
const int muxS = 8; // mux common pin

const int chan0_button = 9; // mux channel 0 replacement

uint16_t button_bounce_state[3] = {0}; // Buffer for button debounce
int button_bounce_idx = 0; // An index in the button_bounce_state array

void mux_init() {
  pinMode(mux0, OUTPUT);
  pinMode(mux1, OUTPUT);
  pinMode(mux2, OUTPUT);
  pinMode(mux3, OUTPUT);
  pinMode(muxS, INPUT);
  pinMode(chan0_button, INPUT);
}

boolean read_mux(byte chan) {
  digitalWrite(mux0, (chan>>0)&1);
  digitalWrite(mux1, (chan>>1)&1);
  digitalWrite(mux2, (chan>>2)&1);
  digitalWrite(mux3, (chan>>3)&1);
  return (digitalRead(muxS) == HIGH);
}

void button_poll() {
  unsigned long now = millis();
  debounce_time = now - debounce_time_old;

  if (debounce_time > debounce_interval) {
    debounce_time_old = now;
    last_button_state = button_state;

    uint16_t buf = 0;
    // Mux channel 0 is bad, its switch is
    // wired directly to an arduino pin.
    if (digitalRead(chan0_button)) {
      buf |= 1 << 0;
    } else {
      buf &= ~(1 << 0);
    }
    for (int i=1; i<16; i++) {
      if (read_mux(i)) {
        buf |= 1 << i;
      } else {
        buf &= ~(1 << i);
      }
    }
    button_bounce_state[button_bounce_idx] = buf;
    button_bounce_idx++;
    if (button_bounce_idx >= 3) {
      button_bounce_idx = 0;
    }

    uint16_t bounced = 0xffff;
    for (int i=0; i<3; i++) {
      bounced = bounced & button_bounce_state[i];
    }

    last_button_state = button_state;
    button_state = bounced;

    uint16_t edge = 0;
    edge = ~last_button_state & button_state; // Mask of all changed inputs
    edge = edge & button_state; // All inputs that are now high
    if (edge != 0) {
      for (int i=0; i<16; i++) {
        if ((edge >> i) & 1) {
          display_status[i/4] = i;
        }
      }
    }
  }
}
