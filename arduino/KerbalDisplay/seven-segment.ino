#include "EngNumber.h"

/* The LEDs on the displays are set up as a matrix.
   x is always the id of the display:
   x=0 - bottom right
   x=1 - bottom left
   x=2 - top right
   x=3 - top left

   y goes from 0 to 7:
   y=7 - last (rightmost) button
   y=6 - K exponent
   y=5 - M exponent
   y=4 - G exponent
   y=3 - -ve light
   y=2 - first button (leftmost)
   y=1 - second button
   y=0 - third button
*/

// Keeping track of display exponents
const int KILO=3;
const int MEGA=6;
const int GIGA=9;

int demonumcounter = 0;
void seven_segment_init() {
  for (int i=0; i<displays.getDeviceCount(); i++) {
    displays.shutdown(i, false);
    displays.clearDisplay(i);
    displays.setIntensity(i, 3);
  }
}

void update_display_1() {
  EngNumber d1;
  int x = 0;
  byte row = B00000000;

  switch (display_status[0]) {
  case MUX_PET:
    row |= 1 << 2;
    floatToEng(VData.TPe, &d1);
    break;
  case MUX_OXS:
    row |= 1 << 1;
    floatToEng(VData.OxidizerS, &d1);
    break;
  case MUX_NODEDV:
    row |= 1 << 0;
    floatToEng(VData.MNDeltaV, &d1);
    break;
  case MUX_RALT:
    row |= 1 << 7;
    floatToEng(VData.RAlt, &d1);
    break;
  default:
    row = 0;
    floatToEng(-1, &d1);
    break;
  }

  // Set the exponent
  switch (d1.exponent) {
  case 0:
    break;
  case KILO:
    row |= 1 << 6;
    break;
  case MEGA:
    row |= 1 << 5;
  case GIGA:
    row |= 1 << 4;
  default:
    row |= 1 << 6;
    row |= 1 << 5;
    row |= 1 << 4;
  }

  if (d1.negative) {
    row |= 1 << 3;
  }

  displays.setRow(2, 0, row);

  // Draw the numbers
  for (int i=0; i<4; i++) {
    displays.setChar(0, i, d1.digits[i], d1.dp == i);
  }
}

void update_display_2() {
  EngNumber d2;
  int x=1;
  byte row = B00000000;

  switch (display_status[1]) {
  case MUX_PERI:
    row |=1 << 2;
    // Show the far periapsis if we're not in stable orbit
    if (orbit_state == STATE_ORBIT) {
      floatToEng(VData.PE, &d2);
    } else {
      floatToEng(VData.FarPE, &d2);
    }
    break;
  case MUX_NODET:
    row |= 1 << 1;
    floatToEng(VData.MNTime, &d2);
    break;
  case MUX_LFS:
    row |= 1 << 0;
    floatToEng(VData.LiquidFuelS, &d2);
    break;
  case MUX_ALT:
    row |= 1 << 7;
    floatToEng(VData.Alt, &d2);
    break;
  default:
    row = 0;
    floatToEng(-1, &d2);
    break;
  }

  // Set the exponent
  switch (d2.exponent) {
  case 0:
    break;
  case KILO:
    row |= 1 << 6;
    break;
  case MEGA:
    row |= 1 << 5;
    break;
  case GIGA:
    row |= 1 << 4;
    break;
  default:
    row |= 1 << 6;
    row |= 1 << 5;
    row |= 1 << 4;
    break;
  }

  if (d2.negative) {
    row |= 1 << 3;
  }

  displays.setRow(2, 1, row);

  // Draw the numbers
  for (int i=0; i<4; i++) {
    displays.setChar(1, i, d2.digits[i], d2.dp == i);
  }
}

void update_display_3() {
  EngNumber d3;
  byte row = B00000000;

  switch (display_status[2]) {
  case MUX_APT:
    row |=1 << 2;
    floatToEng(VData.TAp, &d3);
    break;
  case MUX_VERTV:
    row |=1 << 1;
    floatToEng(VData.VVI, &d3);
    break;
  case MUX_ELECTRIC:
    row |=1 << 0;
    floatToEng(VData.ECharge, &d3);
    break;
  case MUX_INC:
    row |=1 << 7;
    floatToEng(VData.inc, &d3);
    break;
  default:
    row = 0;
    floatToEng(-1, &d3);
    break;
  }

  // Set the exponent
  switch (d3.exponent) {
  case 0:
    break;
  case KILO:
    row |= 1 << 6;
    break;
  case MEGA:
    row |= 1 << 5;
    break;
  case GIGA:
    row |= 1 << 4;
    break;
  default:
    row |= 1 << 6;
    row |= 1 << 5;
    row |= 1 << 4;
    break;
  }

  if (d3.negative) {
    row |= 1 << 3;
  }

  displays.setRow(2, 2, row);

  // Draw the numbers
  for (int i=0; i<4; i++) {
    displays.setChar(0, i+4, d3.digits[i], d3.dp == i);
  }
}

void update_display_4() {
  EngNumber d4;
  byte row = B00000000;

  switch (display_status[3]) {
  case MUX_APO:
    row |=1 << 2;
    floatToEng(VData.AP, &d4);
    break;
  case MUX_SURFV:
    row |=1 << 1;
    floatToEng(VData.Vsurf, &d4);
    break;
  case MUX_MONO:
    row |=1 << 0;
    floatToEng(VData.MonoProp, &d4);
    break;
  case MUX_G:
    row |=1 << 7;
    floatToEng(VData.G, &d4);
    break;
  default:
    row = 0;
    floatToEng(-1, &d4);
    break;
  }

  // Set the exponent
  switch (d4.exponent) {
  case 0:
    break;
  case KILO:
    row |= 1 << 6;
    break;
  case MEGA:
    row |= 1 << 5;
    break;
  case GIGA:
    row |= 1 << 4;
    break;
  default:
    row |= 1 << 6;
    row |= 1 << 5;
    row |= 1 << 4;
    break;
  }

  if (d4.negative) {
    row |= 1 << 3;
  }

  displays.setRow(2, 3, row);

  // Draw the numbers
  for (int i=0; i<4; i++) {
    displays.setChar(1, i+4, d4.digits[i], d4.dp == i);
  }
}

/* From left to right, bit numbers in a row look as follows:
    1
   6 2
    7
   5 3
    4 0

  With that in mind, here's a handy list of letters:
  A = B01110111
  b = B00011111
  B = B01111111
  c = B00001101
  C = B01001110
  d = B00111101
  E = B01001111
  F = B01000111
  G = B01011110
  h = B00010111
  H = B00110111
  i = B00010000
  I = B00110000
  J = B00111100
  L = B00001110
  l = B00000110 (reversed 1, work with me here)
  n = B00010101
  o = B00011101
  O = B01111110
  P = B01100111
  r = B00000101
  S = B01011011 (indistinguishable from 5)
  t = B00001111
  u = B00011100
  U = B00111110
  y = B00111011
  Z = B01101101 (indistinguishable from 2)
*/

const byte numMessages = 11;
const byte messages[numMessages][8] = {
  { B00011101, B00011100, B10001111, B00110000,
    B00011100, B00010101, B00001101, B00010111 }, // out.lunch
  { B01111111, B01001111, B01001111, B01100111,
    B01111111, B01001111, B01001111, B01100111 }, // BEEP BEEP
  { B01100111, B01001111, B00001111, B01001111,
    B00110111, B00000101, B00111101, B00111011 }, // PEtE Hrdy
  { B01000111, B00000101, B01001111, B01001111,
    B00110111, B00111110, B01011110, B01011011 }, // FREE HUGS
  { B00001110, B01110111, B00011100, B00010101,
    B00001101, B10010111, B00010000, B00001111 }, // LAunch.it
  { B00111100, B01001111, B11111111, B00000101,
    B00111110, B00001110, B01001111, B01011011 }, // JEB.rULES
  { B01111111, B00110000, B00001110, B00001110,
    B01111111, B01001111, B01011011, B00001111 }, // BILL BESt
  { B01111111, B01111110, B11111111, B00000101,
    B00111110, B00001110, B01001111, B01011011 }, // BOB.rULES
  { B00010101, B01111110, B01111110, B00011111,
    B01000111, B00011101, B00011101, B00111101 }, // n00b Food
  { B01011011, B00010101, B01110111, B01001110,
    B11011011, B01100111, B00001110, B01011011 }, // SnACS.PLS
  { B01001110, B00000101, B01110111, B01011011,
    B00110111, B00000000, B00010000, B00001111 } // CrASH it
};
const int messageTime = 30000;
unsigned long messageUpdate = 0;

void numeric_demo() {
  displays.setRow(1, 4, B00000101); // r
  displays.setRow(1, 5, B00011101); // o
  displays.setRow(1, 6, B00011111); // b
  displays.setRow(1, 7, B00011101); // o
  displays.setRow(0, 4, B00111101); // d
  displays.setRow(0, 5, B00010000); // i
  displays.setRow(0, 6, B00010101); // n
  displays.setRow(0, 7, B00011101); // o

  unsigned long now = millis();
  if (messageUpdate < now) {
    int messageId = random(numMessages);
    
    displays.setRow(1, 0, messages[messageId][0]);
    displays.setRow(1, 1, messages[messageId][1]);
    displays.setRow(1, 2, messages[messageId][2]);
    displays.setRow(1, 3, messages[messageId][3]);
    displays.setRow(0, 0, messages[messageId][4]);
    displays.setRow(0, 1, messages[messageId][5]);
    displays.setRow(0, 2, messages[messageId][6]);
    displays.setRow(0, 3, messages[messageId][7]);
    messageUpdate = now + messageTime;
  }
}

void numeric_off() {
  displays.clearDisplay(0);
  displays.clearDisplay(1);
  displays.clearDisplay(2);
}

void numeric_display() {
  update_display_1();
  update_display_2();
  update_display_3();
  update_display_4();
}
