/* KerbalController
   Arduino sketch for a KSP control panel.
   Interfaces with the KSPSerialIO mod. Based on
   zitronen's example code.
   Refer to documentation for requirements and pinouts.

   Peter Hardy <peter@hardy.dropbear.id.au>
*/

// The brightness of exposed LEDs. Don't use this
// brightness for ones in the annunciator, they're
// well covered.
const byte colour_value = 128;

// Locations of components of the Action Group VData field
const byte AGSAS = 0; // SAS action group
const byte AGRCS = 1; // RCS action group
const byte AGLight = 2; // Light action group
const byte AGGear = 3; // Gear action group
const byte AGBrakes = 4; // Brakes action group
const byte AGAbort = 5; // Abort action group
const byte AGCustom01 = 6; // Custom action group 1
const byte AGCustom02 = 7;
const byte AGCustom03 = 8;
const byte AGCustom04 = 9;
const byte AGCustom05 = 10;
const byte AGCustom06 = 11;
const byte AGCustom07 = 12;
const byte AGCustom08 = 13;
const byte AGCustom09 = 14;
const byte AGCustom10 = 15;

const byte display_interval = 50; // How often to update the display
const byte demo_interval = 500; // How often to update the display in demo mode
const int slowBlinkRate = 1000; // How frequently to blink slow blinkers
const int fastBlinkRate = 500; // How frequently to blink fast blinkers
bool slowBlinkState = false; // Current state of slow blinking lights
bool fastBlinkState = false; // Current state of fast blinking lights
long lastSlowBlink = 0; // Last time slow blink changed
long lastFastBlink = 0; // Last time fast blnk changed


byte controlStatus(byte n) {
  return ((VData.ActionGroups >> n) & 1) == 1;
}

void blinkUpdate() {
  long now = millis();
  if (now > lastSlowBlink + slowBlinkRate) {
    lastSlowBlink = now;
    slowBlinkState = !slowBlinkState;
  }
  if (now > lastFastBlink + fastBlinkRate) {
    lastFastBlink = now;
    fastBlinkState = !fastBlinkState;
  }
}

void stageLedSet() {
  // If abort button has been hit, blink fast.
  // TODO: Get stage lock status back here.
  // Otherwise, set the colour depending on whether stage lock is on.
  if (controlStatus(AGAbort)) {
    if (fastBlinkState) {
      leds[stageStateLed] = CRGB::Red;
    } else {
      leds[stageStateLed] = CRGB::Black;
    }
  } else {
    /* if (readMuxPin(stageLockPin)) { */
    /*   leds[stageStateLed] = CRGB::Green; */
    /* } else { */
    /*   leds[stageStateLed] = CRGB::Red; */
    /* } */
    leds[stageStateLed] = CRGB::Green;
  }
}

void alarmLedSet() {
  // Wipe from green to red depending on how much fuel is in current stage.
  // Blink if empty.
  // If solid fuel, use that. Otherwise use monoprop.
  // Off if no fuel at all.
  float totalFuel, remainingFuel;
  if (VData.LiquidFuelTotS > 0) {
    totalFuel = VData.LiquidFuelTotS;
    remainingFuel = VData.LiquidFuelS;
  } else if (VData.MonoPropTot > 0) {
    totalFuel = VData.MonoPropTot;
    remainingFuel = VData.MonoProp;
  } else {
    leds[throttleAlarmLed] = CRGB::Black;
    return;
  }
  // Can't guarantee fuel remaining will actually get to 0...
  if (remainingFuel < 0.1) {
    if (slowBlinkState) {
      leds[throttleAlarmLed] = CRGB::Red;
    } else {
      leds[throttleAlarmLed] = CRGB::Black;
    }
  } else {
    // FastLED's Rainbow colour map puts red at 0 and green at 96
    int hue = int(remainingFuel / totalFuel * 96);
    leds[throttleAlarmLed] = CHSV(hue, 255, 255);
  }
}

/* void gearLedSet() { */
/*   if (controlStatus(AGGear)) { */
/*     leds[gearLed] = CHSV(160, 255, colour_value); */
/*   } else { */
/*     leds[gearLed] = CRGB::Black; */
/*   } */
/* } */

/* void brakeLedSet() { */
/*   if (controlStatus(AGBrakes)) { */
/*     leds[brakeLed] = CHSV(32, 255, colour_value); */
/*   } else { */
/*     leds[brakeLed] = CRGB::Black; */
/*   } */
/* } */

/* void lightLedSet() { */
/*   if (controlStatus(AGLight)) { */
/*     leds[lightsLed] = CRGB::Blue; */
/*   } else { */
/*     leds[lightsLed] = CRGB::Black; */
/*   } */
/* } */

void rcsLightSet() {
  if (controlStatus(AGRCS)) {
    // Wipe from green to red depending on how much mono is available
    int hue = int(VData.MonoProp / VData.MonoPropTot * 96);
    leds[rcsLed] = CHSV(hue, 255, colour_value);
  } else {
    leds[rcsLed] = CRGB::Black;
  }
}

void sasLightSet() {
  if (controlStatus(AGSAS)) {
    leds[sasLed] = CHSV(140, 255, colour_value);
  } else {
    leds[sasLed] = CRGB::Black;
  }
}

void solidLightSet() {
  if (VData.SolidFuelTot > 0) {
    int hue = int(VData.SolidFuel / VData.SolidFuelTot * 96);
    leds[solidLed] = CHSV(hue, 255, 255);
  } else {
    leds[solidLed] = CRGB::Black;
  }
}

void oxygenLightSet() {
  if (VData.OxidizerTotS > 0) {
    int hue = int(VData.OxidizerS / VData.OxidizerTotS * 96);
    leds[oxygenLed] = CHSV(hue, 255, 255);
  } else {
    leds[oxygenLed] = CRGB::Black;
  }
}

void liquidLightSet() {
  if (VData.LiquidFuelTotS > 0) {
    int hue = int(VData.LiquidFuelS / VData.LiquidFuelTotS * 96);
    leds[liquidLed] = CHSV(hue, 255, 255);
  } else {
    leds[liquidLed] = CRGB::Black;
  }
}

void xenonLightSet() {
  if (VData.XenonGasTot > 0) {
    int hue = int(VData.XenonGas / VData.XenonGasTot * 96);
    leds[xenonLed] = CHSV(hue, 255, 255);
  } else {
    leds[xenonLed] = CRGB::Black;
  }
}

void powerLightSet() {
  int remainingPowerPercent = int(VData.ECharge / VData.EChargeTot * 100);
  if (remainingPowerPercent < 5) {
    if (fastBlinkState) {
      leds[powerLed] = CHSV(0, 255, 255);
    } else {
      leds[powerLed] = CHSV(45, 255, 255);
    }
  } else if (remainingPowerPercent < 10) {
    if (slowBlinkState) {
      leds[powerLed] = CHSV(0, 255, 255);
    } else {
      leds[powerLed] = CHSV(45, 255, 255);
    }
  } else if (remainingPowerPercent < 20) {
    leds[powerLed] = CHSV(45, 255, 255);
  } else {
    leds[powerLed] = CRGB::Black;
  }
}

void heatLightSet() {
  if (VData.MaxOverHeat > 95) {
    if (fastBlinkState) {
      leds[powerLed] = CHSV(10, 0, 0);
    } else {
      leds[powerLed] = CRGB::Black;
    }
  } else if (VData.MaxOverHeat > 90) {
    leds[powerLed] = CHSV(10, 0, 0);
  } else if (VData.MaxOverHeat > 80) {
    leds[powerLed] = CHSV(64, 255, 255);
  }
}

void chuteLightSet() {
  // I think this means it'll only fire if we're going downwards:
  if (VData.VVI < 0) {
    if (VData.MachNumber > 0.85) {
      // "unsafe"
      if (slowBlinkState) {
        leds[chutesLed] = CHSV(0, 255, 255);
      } else {
        leds[chutesLed] = CRGB::Black;
      }
    } else if (VData.MachNumber > 0.8) {
      // "risky"
      leds[chutesLed] = CHSV(10, 255, 255);
    } else if (VData.MachNumber > 0.1) {
      leds[chutesLed] = CHSV(96, 255, 255);
    } else {
      leds[chutesLed] = CRGB::Black;
    }
  }
}

void orbitLightsSet() {
  /* SOI numbers are three decimal digits:
     First digit indicates the sun we're orbiting.
     Second digit indicates the planet.
     Third digit indiciates the moon of that planet.
  */
  if (VData.FarSOINumber == 0) { // Stable orbit.
    orbit_state = STATE_ORBIT;

    leds[interceptLed] = CRGB::Black;
    leds[escapeLed] = CRGB::Black;
  } else if ((VData.SOINumber/10) == (VData.FarSOINumber/10)) { // Transitioning between a planet and one of its moons
    if (VData.FarSOINumber > VData.SOINumber) { // Planet -> moon
      orbit_state = STATE_INTERCEPT;
      leds[interceptLed] = CHSV(160, 255, 255);
      leds[escapeLed] = CRGB::Black;
    } else { // Moon -> Planet
      orbit_state = STATE_ESCAPE;
      leds[interceptLed] = CRGB::Black;
      leds[escapeLed] = CHSV(160, 255, 255);
    }
  } else {
    if (VData.FarSOINumber == 100) { // Escaping to Kerbol orbit
      orbit_state = STATE_ESCAPE;
      leds[interceptLed] = CRGB::Black;
      leds[escapeLed] = CHSV(32, 255, 255);
    } else { // This should only happen if we're going from one planet to another
      orbit_state = STATE_INTERCEPT;
      leds[interceptLed] = CHSV(32, 255, 255);
      leds[escapeLed] = CHSV(32, 255, 255);
    }
  }
}

void demoMode() {
  leds[random(fastLedNum)] = CHSV(random(255), 255, 255);
}

void display() {
  unsigned long now = millis();
  switch (DPacket.state) {
  case DISPLAY_DEMO:
    if (display_time > demo_interval) {
      update_demo(now);
    }
    now = millis();
    display_time = now - display_time_old;
    if (display_time > demo_interval) {
      update_demo(now);
    }
    break;
  case DISPLAY_ON:
    display_time = now - display_time_old;
    if (display_time > display_interval) {
      update_display(now);
    }
    break;
  default:
    for (int i=0; i<fastLedNum; i++) {
      leds[i] = CRGB::Black;
    }
    FastLED.show();
    numeric_off();
    break;
  }
}

void update_demo(unsigned long now) {
  demoMode();
  FastLED.show();
  numeric_demo();
  display_time_old = now;
}

void update_display(long now) {
  blinkUpdate();
  stageLedSet();
  alarmLedSet();
  //gearLedSet();
  //brakeLedSet();
  //lightLedSet();
  rcsLightSet();
  sasLightSet();
  solidLightSet();
  oxygenLightSet();
  liquidLightSet();
  xenonLightSet();
  powerLightSet();
  heatLightSet();
  chuteLightSet();
  orbitLightsSet();

  numeric_display();
  FastLED.show();
}
