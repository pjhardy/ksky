/* KerbalNavball

   Displaying a KSP navball on an ST7565 LCD display.
   Based on Matt Casey's navball rendering demo:
   https://github.com/Catmacey/KerbalControl
*/

#include <SPI.h>

#include <RA8875.h>

#include "data_packets.h"

#include "types.h"
#include "custom_chars.h"
#include "model_mega.h"

// Library only supports hardware SPI at this time
// Connect SCLK to UNO Digital #13 (Hardware SPI clock)
// Connect MISO to UNO Digital #12 (Hardware SPI MISO)
// Connect MOSI to UNO Digital #11 (Hardware SPI MOSI)
#define RA8875_INT 3
#define RA8875_CS 10
#define RA8875_RESET 9

const int framedelay = 100; // hopefully 10fps
unsigned long nextframe = 0; // when the last frame was drawn
enum RA8875boolean visibleLayer = LAYER1;

RA8875 display = RA8875(RA8875_CS, RA8875_RESET);

ControlPacket CPacket;
VesselData VData;

bool Connected=true;

void setup() {
  display.begin(Adafruit_800x480, 8);
  display.useLayers(true);
  display.displayOn(true);
  display.GPIOX(true); // Enable TFT - display enable tied to GPIOX
  //display.PWM1config(true, RA8875_PWM_CLK_DIV1024); // PWM output for backlight
  //display.PWM1out(255);
  display.fillWindow();
  upload_user_characters();

  // Dummy values for debug output
  CPacket.MainControls = 0x01;
  CPacket.Mode = 0x23;
  CPacket.ControlGroup = 0x6745;
  CPacket.AdditionalControlByte1 = 0x89;
  CPacket.AdditionalControlByte2 = 0xab;
  CPacket.Pitch = 0xefcd;
  CPacket.Roll = 0x2301;
  CPacket.Yaw = 0x6745;
  CPacket.TX = 0xab89;
  CPacket.TY = 0xefcd;
  CPacket.TZ = 0x2301;
  CPacket.Throttle = 0x6745;

  VData.Pitch = 0;
  VData.Heading = 180;
  VData.Roll = 0;
}

void loop() {
  uint8_t idx = 0;
  uint8_t effectCount = 0;
  int axisidx, btn2db, timer, fps, addr = 0;
  uint16_t ctr;
  float tmp;
  float degToRad = PI / 180;
  float axis[3]; // x, y, z

  unsigned long now = millis();
  if (now > nextframe) {
    nextframe = now + framedelay;
    axis[0] = VData.Pitch; // Pitch already -180 to 180
    axis[1] = 180 - VData.Heading; // Heading is 0 to 360
    axis[2] = VData.Roll; // Roll already -180 to 180
    if (visibleLayer == LAYER1) {
      display.writeTo(L2);
      transform(axis[0]*degToRad, axis[1]*degToRad, axis[2]*degToRad);
      display.layerEffect(LAYER2);
      visibleLayer = LAYER2;
    } else {
      display.writeTo(L1);
      transform(axis[0]*degToRad, axis[1]*degToRad, axis[2]*degToRad);
      display.layerEffect(LAYER1);
      visibleLayer = LAYER1;
    }

    display.setFontScale(2);
    display.setTextColor(RA8875_RED);
    // Centre of level indicator is at (32, 12)
    display.setCursor(368, 228);
    display.showUserChar(LEVEL_CHAR, 7);
    display.setCursor(368, 244);
    display.showUserChar(LEVEL1_CHAR, 7);
    // Overlay the direction indicator
    //display.drawBitmap(88, 30, g_direction_mask, 17, 8, RA8875_WHITE);
    //display.drawBitmap(88, 30, g_direction, 17, 8, RA8875_WHITE);
    // Overlay the 3d with a bezel image
    //display.drawBitmap(32, 0, g_panel_overlay, 128, 64, RA8875_WHITE);

    VData.Pitch = VData.Pitch+3;
    if (VData.Pitch > 180) VData.Pitch = -179;
    VData.Heading = VData.Heading+2;
    if (VData.Heading > 360) VData.Heading = 1;
    VData.Roll = VData.Roll+1;
    if (VData.Roll > 180) VData.Roll = -179;
  }
}

void upload_user_characters() {
  uint8_t buf[16];
  // The first 16 characters are an 8x2 grid for the level indicator
  for (int i=0; i<16; i++) {
    memcpy_P(&buf, &level_indicator[i], 16);
    display.uploadUserChar(buf, i);
  }
}
