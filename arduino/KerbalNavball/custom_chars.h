// Custom characters used to represent navball icons
#include "level_icon.h"
#include "prograde_icons.h"
#include "normal_icons.h"
#include "radial_icons.h"
#include "target_icons.h"
#include "maneuver_icon.h"

enum custom_chars {
  LEVEL_CHAR = 0,
  LEVEL1_CHAR = 8,
  PROGRADE_CHAR = 16,
  PROGRADE1_CHAR = 20,
  RETROGRADE_CHAR = 24,
  RETROGRADE1_CHAR = 28,
  NORMAL_CHAR = 32,
  NORMAL1_CHAR = 36,
  ANTINORMAL_CHAR = 40,
  ANTINORMAL1_CHAR = 44,
  RADIALIN_CHAR = 48,
  RADIALIN1_CHAR = 52,
  RADIALOUT_CHAR = 56,
  RADIALOUT1_CHAR = 60,
  TARGETPROGRADE_CHAR = 64,
  TARGETPROGRADE1_CHAR = 68,
  TARGETRETROGRADE_CHAR = 72,
  TARGETRETROGRADE1_CHAR = 76,
  MANEUVERPROGRADE_CHAR = 84
};

const uint8_t PROGMEM empty[16] = {
  0b00000000, //  0
  0b00000000, //  1
  0b00000000, //  2
  0b00000000, //  3
  0b00000000, //  4
  0b00000000, //  5
  0b00000000, //  6
  0b00000000, //  7
  0b00000000, //  8
  0b00000000, //  9
  0b00000000, // 10
  0b00000000, // 11
  0b00000000, // 12
  0b00000000, // 13
  0b00000000, // 14
  0b00000000  // 15
};
