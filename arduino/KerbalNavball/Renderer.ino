// Routine to draw and calc 3d transform of the model
// Based on 3D Cube by Jason Wright (C)opyright Pyrofer 2006
// http://www.pyrofersprojects.com/blog/3dpic/
// Modified by Matt Casey.
// Added use of structs for storage of model data (verts, lines, tris)
// Added halftone filled triangles.
// Added routine to cull hidden lines/tris using crude z technique

void transform(float rotx, float roty, float rotz) {
  uint8_t idx; // temp variable for loops
  float xt, yt, zt, x, y, z, sinax, cosax, sinay, cosay, sinaz, cosaz; // work variables

  // Not interested in translation
  float xpos = 0;
  float ypos = 0;
  float zpos = 256;

  Vertex_t vert;
  Line_t line;
  Face_t face;

  sinax = sin(rotx); // precalculate the sin and cos values
  cosax = cos(rotx); // for the rotation as this saves a
  sinay = sin(roty); // little time when running as we
  cosay = cos(roty); // call sin and cos less often
  sinaz = sin(rotz); // they are slow routines
  cosaz = cos(rotz); // and we don't want slow!

  // translate 3D vertex position to 2D screen position
  for (idx=0; idx<g_model.vertcount; idx++) {
    memcpy_P(&vert, &g_model_verts[idx], sizeof(vert));
    x = vert.x;
    y = vert.y;
    z = vert.z;

    // rotate around the Y axis: Heading
    xt = x*cosay - z*sinay;
    zt = x*sinay + z*cosay; // using X and Z
    x = xt;
    z = zt;

    // rotate around the X axis: Pitch
    yt = y*cosax - z*sinax;
    zt = y*sinax + z*cosax; // using Y and Z
    y = yt;
    z = zt;

    // rotate around the Z axis: Roll
    xt = x*cosaz - y*sinaz;
    yt = x*sinaz + y*cosaz; // using X and Y
    x = xt;
    y = yt;

    x += xpos; // add the object position offset
    y += ypos; // for both x and y

    // I want to know if the point is in the front or bck half of
    // the model in order to cull hidden lines
    g_pointBuffer[idx].z = z;

    z += (OFFSETZ-zpos); // as well as Z

    // Translate 3d to 2d coordinates for screen
    g_pointBuffer[idx].x = (x*256/z)+OFFSETX;
    g_pointBuffer[idx].y = (y*256/z)+OFFSETY;

    // Draw vertex indexes for debugging
    // Handy but very cluttered for complex models
    //display.setCurser(newx[idx],newy[idx]);
    //display.print(idx, DEC);
  }

  display.fillWindow();
  display.fillCircle(400, 240, 124, RA8875_LIGHT_GREY);
  display.drawCircle(400, 240, 125, RA8875_WHITE);

  // Draw the faces (triangles)
  // Only for triangles where all vertexes are
  // towards the front of the model
  for(idx=0; idx<g_model.facecount; idx++) {
    memcpy_P(&face, &g_model_faces[idx], sizeof(face));
    if (
        g_pointBuffer[face.a-1].z > -5
        &&
        g_pointBuffer[face.b-1].z > -5
        &&
        g_pointBuffer[face.c-1].z > -5
        ) {
      // Modified fill : colour 4 produces nice dots
      display.fillTriangle(g_pointBuffer[face.a-1].x,
                           g_pointBuffer[face.a-1].y,
                           g_pointBuffer[face.b-1].x,
                           g_pointBuffer[face.b-1].y,
                           g_pointBuffer[face.c-1].x,
                           g_pointBuffer[face.c-1].y,
                           RA8875_LIGHT_ORANGE);
    }
  }

  // draw the lines that make up the object
  // Only for lines where both vertexes are towards
  // the front of the model in it's current transform
  for (idx=0; idx<g_model.linecount; idx++) {
    memcpy_P(&line, &g_model_lines[idx], sizeof(line));
    if (
        g_pointBuffer[line.startvert-1].z > 5
        &&
        g_pointBuffer[line.endvert-1].z > 5
        ) {

      display.drawLine(g_pointBuffer[line.startvert-1].x,
                       g_pointBuffer[line.startvert-1].y,
                       g_pointBuffer[line.endvert-1].x,
                       g_pointBuffer[line.endvert-1].y,
                       RA8875_WHITE);
    }
  }
}
