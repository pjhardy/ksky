#ifndef __types__
#define __types__

#include <stdint.h>

//#define OFFSETX 96
//#define OFFSETY 32
#define OFFSETX 400
#define OFFSETY 240
#define OFFSETZ 30

#include "g_splashscreen.h"
#include "g_panel_overlay.h"
#include "g_direction.h"
#include "g_direction_mask.h"

// Set of vertexes in a model
typedef struct {
  int x;
  int y;
  int z;
} Vertex_t;

// Triplet of Vertex indexes that form a face
typedef struct {
  uint8_t a;
  uint8_t b;
  uint8_t c;
} Face_t;

// Set of lines as verex indexes (start, end)
typedef struct {
  uint8_t startvert;
  uint8_t endvert;
} Line_t;

// Model made up of vertexes and lines
typedef struct {
  const Vertex_t *verts;
  const Line_t *lines;
  const Face_t *faces;
  const uint8_t vertcount;
  const uint8_t linecount;
  const uint8_t facecount;
} Model_t;

// Set of 2d screen points transformed from the vertexes
typedef struct {
  int x;
  int y;
  int z;
} Point_t;

// Buffer of transformed vertexes
extern Point_t g_pointBuffer[140];

#endif
