#include <FastLED.h>

const int vertSpeedPin = 5;
const int radarAltPin = 6;

const int fastLedNum = 2;
const int fastLedPin = 4;
CRGB leds[fastLedNum];

boolean up=true;

void displayInit() {
  pinMode(vertSpeedPin, OUTPUT);
  pinMode(radarAltPin, OUTPUT);
  analogWrite(vertSpeedPin, 0);
  analogWrite(radarAltPin, 0);

  FastLED.addLeds<WS2812B, fastLedPin>(leds, fastLedNum);
  for (int i=0; i<fastLedNum; i++) {
    leds[i] = CRGB::Black;
  }
  FastLED.show();
}

void setup() {
  displayInit();
}

void loop() {
  for (int i=0; i<256; i++) {
    int val;
    if (up) {
      val = i;
    } else {
      val = 255-i;
    }
    analogWrite(vertSpeedPin, val);
    analogWrite(radarAltPin, val);
    for (int x=0; x<fastLedNum; x++) {
      leds[x] = CHSV(val, 255, 255);
    }
    FastLED.show();
    delay(20);
  }
  up = !up;
}
