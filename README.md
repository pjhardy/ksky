# KSKY #
This repository contains code, panel layouts and documentation for KSKY, a
__K__erbal Space Program __s__creen and __k__e__y__board. The project is a
hardware control interface and display device for
[KSP](https://kerbalspaceprogram.com/). It interfaces with the game using
[KSPSerialIO](http://forum.kerbalspaceprogram.com/threads/66393-Hardware-Plugin-Arduino-based-physical-display-serial-port-io-tutorial-%2808-Oct%29).
The KSPSerialIO mod, and parts of the arduino code here, are written by
zitronen.

## Hardware ##
This system has three main components:

* Game I/O and most of the input handling is done with an Arduino Mega2560,
with a Mux Shield II and another custom shield terminating connections to the
individual panels.
* The digital displays are driven by an Arduino Leonardo and some custom PCBs.
* A Teensy 3.2 drives a TFT display through an RA8875 controller.

A full hardware list is in `BOM.md`.

Interface elements are grouped on to a number of console panels. I've
written a [blog post](http://hardy.dropbear.id.au/blog/2014/11/making-laser-cut-backlit-control-panels) describing how these are fabricated, and some notes
on how panels for this project should be deisgned are in `NOTES.md`.

`PANELS.md` contains details of the panels I've constructed for this project,
and DXF files for them are in the `panels` directory.

## Wiring notes ##
Most input devices are just wired directly to an input pin, with a 10K
pull-down resistor. Potentiometers wired directly to their appropriate
analogue input pin. A full list of input pins on the Arduino and Mux
Shield is in `PINOUT.md`.

Most standalone LEDs so far are WS2812B addressable LEDs. They share 5V
and GND with their appropriate panel, and DIN and DOUT pins are wired in
series to form a single bus.

Schematics and PCBs were created using [EAGLE](http://www.cadsoftusa.com/) 7.
The freeware license is fine for viewing and editing these designs.

### Schematics warning ###
The current version of the PCB schematics have a couple of severe bugs.

* The SparkFun footprint for the seven segment displays is around 8mm
shorter than the component. This resulted in the actual displays overlapping
the footprints for the LEDs along the edges. On my 1.0 boards I was able
to bend the LED pins to the side to compensate. But I've created a quick
1.1 revision of the display boards to fix it.
* The IDC connector pins on the display and controller boards were mismatched.
They're unusable unless you're prepared to hack up custom IDC cables or
similar. I haven't yet tried to fix this.
* The two MAX7219 ICs that drive the seven-sgment displays just... don't have
the DP pin connected to anything. The IDC connectors have their DP pins
connected together, but I just plain didn't add a net from them to the
MAX7219 pin. That's easily fixed with a couple of short lengths of wire,
but it sure was startling to discover.

## Arduino code ##
The following libraries are required to build this code:

* [digitalIOPerformance](https://github.com/projectgus/digitalIOPerformance)
for high speed IO.
* [MuxShield library](http://mayhewlabs.com/products/mux-shield-2), to
drive the Mux Shield.
* [FastLED](http://fastled.io/) animation library, for driving addressable
RGB LEDs.
* [RA8875](https://github.com/sumotoy/RA8875) is used to drive the TFT
controller.

In addition, the following libraries were written to support this project.
* [EngNumber](https://bitbucket.org/pjhardy/engnumber), for converting
floating point numbers to engineering notation.
