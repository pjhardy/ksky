# Panels connecting to the breakout shield

Looking at the shield with the bevelled edge at top. This is pretty-much
upside-down to the writing on the shield.

* 6 pin at top
* 1st 16 pin
* 2nd 16 pin
* 3rd 16 pin
* right-most 6 pin
