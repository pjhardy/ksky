# Designing and building the panels #

## General panel design notes ##

### Required ###
* When PCBs or components need to be attached to panels, use M3 bolts.
  (3.5mm diameter)
* M4 bolts for fixing panels to the chassis. (4.5mm diameter)
* Fillet corners off with 3mm radius.

### Desired ###
* Mounting holes should be 5mmx5mm from edge of panel.
* Try to have mounting holes 10mmx10mm from edge of components.

### Text ###
* All text is Futura.
* 4.5mm text for control titles, 3.5mm for smaller text.

## Workflow ##
I'm using [LibreCAD](http://www.librecad.org/) to design my panels.
[Inkscape](https://inkscape.org/) is used to draw text using TT fonts and
export it as paths that can be used in LibreCAD. It also comes in handy for
converting DXFs in to a format that can work on the laser cutter at my
maker space.

### Adding text to panels ###
* Draw text in Inkscape.
* Scale to appropriate height, move to appropriate zero point -
(0, 0) will be used as the origin in LibreCAD.
* Convert to Path: Path -> Object to Path
* Ungroup: Object -> Ungroup. Keep all letters selected.
* Create a union of all letters: Path -> Union
* Save as DXF.
* Import text in to LibreCAD as a block.
* Explode block, delete block.

LaserCAD can still be a little finicky about importing this. Tweak the
combine and smoothing settings.

### Converting single lines to paths ###
I use single thick line strokes in a few places (notable is the gradations
along the throttle track). These need to be converted to paths to be
used with the laser cutter.

* Load panel in Inkscape and switch to "Edit paths by nodes" (F2).
* Select all the segments of a line. Hit the toolbar button to
  "Convert selected object's stroke to path".
* Go to Path menu -> Union to combine all of the segments. Seems to also
  need to be done even if there's a single segment.

### Preparing for cutting ###
Load final DXF in Inkscape. Move cutting and etching layers to a single
layer, and delete all other layers. Leave cutting edges black, ensure
all etching edges are a different colour.

Check that all text elements fill cleanly. A few times I've ended up
redoing and inserting the text again in Inkscape, just because the
conversion made things look shit.

Save as DXF.

## Cutting ##
Settings that seem to work well for my panels and laser cutter. They
probably won't work for you unless you're using my laser cutter, my
perspex, that has been painted with my paint. By me. Have I stressed
enough that these are a guideline more than an actual rule?

* For cuts, 10mm/sec, 55% power works well.
  * Attempt 2: 5mm/sec, 90%
  * 20160822: 12mm/sec, 80%
* Engraving: 400mm/sec, 50%, 0.03mm scan gap.
  * 20160822: 300mm/sec, 20%, 0.01mm scan gap. Scan gap makes it crisper.

