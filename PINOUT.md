# MUX pinout #

## Bank 1 ##
Digital in

|0|Staging button|
|1|Stage lock key|
|2|Abort switch|
|3|MECO switch|
|4|FTP switch|
|5|Action group 1|
|6|Action group 2|
|7|Action group 3|
|8|Action group 4|
|9|Action group 5|
|10|Action group 6|
|11|Action group 7|
|12|Action group 8|
|13|Action group 9|
|14|Action group 10|

## Bank 2 ##
Digital in

|0|SAS invert|
|1|ROT/SAS|
|2|Translate up|
|3|Translate down|
|4|Translate left|
|5|Translate right|
|6|RCS toggle|
|7|SAS toggle|
|8|Translate forward|
|9|Translate backward|

## Bank 3 ##
Digital out

|0|Throttle motor 1|
|1|Throttle motor 2|

# Arduino pinout #
|3|FastLED data pin|
|5|Throttle motor enable|
|A3|Throttle input pin|

## Mux pins ##
|2|muxS0|
|4|muxS1|
|6|muxS2|
|7|muxS3|
|8|muxOUTMD|
|10|muxIOS1|
|11|muxIOS2|
|12|muxIOS3|
|A0|muxIO1|
|A1|muxIO2|
|A2|muxIO3|
