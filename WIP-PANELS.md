# Panels that aren't going in the main repo yet #

# Overall layout

Centre
* displays at the top
* Brightness and presets very near them

On the right
* right hand attitude control.
* staging
* trackpad?

On the left
* left hand attitude control
* throttle
* keyboard?

## MFD brightness / preset panel ##
A small panel with display brightness and remembering presets for the MFDs.

* Small linear pot for brightness.
* Multi position rotary switch. System remembers which MFD functions were
active for each position on the switch.

### Yet to be determined ###
I still need something for landing gear, brakes, lights. Probably all
on the same panel.
