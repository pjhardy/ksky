# Guide to panels #

## Staging panel ##
Hardware complete.

* Stage lockout key.
* Stage button.
* Abort switch.
* Status light. Red when locked out, green when ready.

## Throttle panel ##
Hardware complete.

* Throttle slider
* Centre-off momentary toggle switch to engage FTP (full throttle position)
  and MECO automatically. Arduino starts sending the appropriate throttle
  level immediately and then moves the slider to the right place.
* ALRM LED will light when current stage is out of fuel and throttle is
  non-zero. It will go off when throttle is zeroed.

## Action groups panel ##
Hardware complete.

10x buttons for action groups. Labelled 1-10. Room on the panel for cards
describing action group bindings, vessel notes, etc.

## Attitude control panels ##

### Left hand panel ###
* In centre of panel, two way joystick.
* To the left:
  * RCS toggle.
  * RCS indicator light. Off when RCS is off. On, changes colour
  according to RCS fuel.
* To the right:
  * SAS toggle

### Right hand panel ###
* In centre of panel, eight way joystick.
* To the left, top to bottom:
  * SAS state LED.
  * SAS momentary toggle.
* To the right, top to bottom:
  * Translation/rotation switch
  * Adjustment knob for strength of translation (and/or rotation?)

## Display panels

There are four display panels. Each one has a "display PCB" with switches and
outputs attached. They all have connections back to a controller board. The
controller has a separate arduino board attached, which talks to the master via
I2C.

* Labels on buttons are 2.5mm high, centre 8mm below centre of buttons.

### LED setup

Each board has 8 LEDs: LED[1-4] are display LEDS. S[1-4]LED are LEDs in the
illuminated switches. Anodes all have separate labelled connections to the
control board. Cathodes all share a common connection.

Anodes on each display board should be connected in parallel on the controller,
and each set connected to pins A-F on a MAX7219. The common cathode from each
controller board should be connected to one of the "digit" pins on the MAX7219.

## Annunciator panel ##

Contains annunciator LEDs showing warning and status of... whatever.

24 LEDs in a convenient package.

### LED housing ###

Housing is cut from 2mm perspex, just a tabbed box with no top, split in to
6x4 sections. I cut and assembled the box, then gave it a few coats of white
spray paint to ensure the inside was reflective, and to try to cover over any
gaps between compartments that were left over. And then drilled holes in the
bottom of each compartment.

An RGB LED is then glued in to the bottom of each compartment. Still using
WS2812B LEDs, so they're arranged in a chain. Each row gets power in parallel
with every other row, and the signal lines are daisychained around the box.

### Assembly ###

I'm planning on gluing a small angle bracket on each end of the LED box.
Between the box and the panel I want to try to use a sheet of regular paper
with labels and whatnot printed on, covered by a thin sheet of clear plastic,
scavenged from an Officeworks folder divider or something.

## Ancillary panels ##

These are designs for other random lasercut pieces I needed during
construction.

### Back cover ###

The design for the plate covering the hole in the back. Includes two USB
ports, so that both Arduinos are accessible without opening. Has a
three-position switch for off, on and demo modes (off turns the lighting
off, but doesn't do much else), and a barrel jack for DC power input.

### Arduino block ###

A plate to mount the Arduino Mega and display controller boards. The extra
holes are for DIN rail mounting clips.

### Power block ###

A plate to mount a buck transformer to supply 5V to the components that
require it, and a terminal block for terminating power rails.
