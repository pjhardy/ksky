# Bill of materials #

## Panel hardware ##
* 3mm translucent acrylic. I used "opal". Half a square metre is more
than you'll ever need.
* Spray paint. I used a matt grey primer that claimed to be suitable for
plastics.
* Nuts and bolts:
    * M4 for bolting panels to the console.
    * M3 for the throttle pot.

## Enclosure ##
* 12mm MDF. I went to my local Bunnings and picked up three 900x450mm sheets.
And used almost all of it.
* Paint. I used a white undercoat (and slathered it all over the interior as
well to increase reflectivity), and a Dulux metallic top coat.
* 4mm self-tapping wood screws, with matching washers. In hindsight, the
mounting holes in my panels may have been a bit too close to the edges, which
made finding washers that fit hard work.

The connections for power and data are still a work in progress:
* 2.1mm DC barrel jack.
* Panel-mounted USB receptacle.
* Small three-position toggle switch to select controller mode.

Internally, I'm currently planning on using an old ATX power supply to
supply power to the board. I've also used a couple of short sections of
DIN rail screwed to the inside of the enclosure, with mounting clips and
scrap 3mm MDF to bolt electronics to.

## Common parts ##
* Arduino-compatible board. I'm using a Mega 2560, because I happened to have
one lying around.
* [Mux Shield II](http://mayhewlabs.com/products/mux-shield-2). I'm using
this shield as an alternative to sourcing components for my own multiplexor.
* 9V power supply. The throttle motor needs 9V to run. I'm powering my
Arduino with a 9V 1.5A supply. Most components are being feed 5V from Vcc
on the Arduino, and the motor is coming from the Vin pin.
* 5mm addressable RGB LEDs. Currently using the clear model, [COM-12999](https://www.sparkfun.com/products/12999) from SparkFun. But I'd like to replace them
with the diffused ones, [COM-12986](https://www.sparkfun.com/products/12986).
* 5mm LED holders. My panels have 11/32" (ugh, imperial) holes for
[COM-11147](https://www.sparkfun.com/products/11147) holders from SparkFun.


## Action groups panel ##
* 10x illuminated momentary push button switches. The ones I bought were
sourced from eBay. The lighting isn't wired up because I was an idiot and
accidentally bought ones that need 250VAC. But loved the mechanical feel
enough to keep them.
    * Button top size 20mm x 15mm.
    * Thread diameter 15.6mm.
    * Full switch size 24mm x 17mm x 40mm.
* The panel was designed to also accomodate 76mm x 127mm system cards
for action group descriptions, notes.

## Throttle panel ##
* Motorised 10k linear slide pot. [COM-10976](https://www.sparkfun.com/products/10976) from SparkFun.
* SN754410 H-Bridge motor driver. This is driving the motor in the slide pot.
* 12mm DPDT momentary centre-off toggle switch. I bought [ST0576](http://www.jaycar.com.au/productView.asp?ID=ST0576) from Jaycar.

## Staging panel ##
* Momentary mushroom-head push button, 22mm mounting hole. Needs NO contacts
at least. Easily sourced from eBay.
* Key switch. I used [COM-10445](https://www.sparkfun.com/products/retired/10445)
from SparkFun, which are now retired. Good, they were pretty cruddy. Looks like
[COM-11473](https://www.sparkfun.com/products/11473) will fit in the same
mounting hole.
* 12mm SPST momentary toggle switch, with protective cover. I used
[ST0577](http://www.jaycar.com.au/productView.asp?ID=ST0577) and
[ST0578](http://www.jaycar.com.au/productView.asp?ID=ST0578) from Jaycar.

## Descent panel ##
* Two DC panel meters. I used [TOL-10285](https://www.sparkfun.com/products/10285) from SparkFun.
* 12mm SPST toggle switch.

## Attitude panels ##
* Two eight-way joysticks. I used a couple of cheap arcade-style sticks from
eBay with embedded microswitches. The trick is finding ones that do not look
like cheap arcade-style sticks from eBay.
  * *Note* that my sticks have an 18mm cutout for the stick, and very oddly
  placed mounting holes. You will almost definitely want to redesign my
  attitude panels.
* Three 6mm SPST toggle switches. I used [STO546](http://www.jaycar.com.au/productView.asp?ID=ST0546) flattened toggles.
* A 10k rotary pot. These are pretty standard devices. My design calls for quite
a narrow knob on it.

## Annunciator ##

The annunciator panel uses a box made from scrap 3mm perspex. Any other
material would substitute in here just fine. There's also 24 of the addressable
LEDs.

## Display panels ##

The displays themselves require:

* Four 4-digit 7-segment displays. These need to be common-cathode displays.
I used [COM-11408](https://www.sparkfun.com/products/11408) from SparkFun.
* 16 illuminated push buttons. Although separate buttons and LEDs would work
too. I used [COM-10443](https://www.sparkfun.com/products/10443) from SparkFun.
* 16 LEDs. I used 3mm blue ones obtained from eBay.

And my display driver requires:
* An Arduino or compatible board. My PCB designs are made for the SparkFun
Pro Micro Arduino-compatible
[DEV-12640](https://www.sparkfun.com/products/12640), but any other board will
work.
* 16 10k pulldown resistors.
* Three MAX7219 LED drivers. Two of these for the 7-segment displays, the other
drives all of the other LEDs in the displays.
* A CD74HC4067 16 channel multiplexer. My PCB designs assume the SSOP footprint.

## Navball ##

The navball display panel currently requires:

* A Teensy microcontroller. I used a Teensy 3.2. The code will technically
run on any Arduino-compatible microcontroller, and I've run previous iterations
on a Uno. But it benefits greatly from something with more CPU power.
* An LCD panel with an RA8875 controller. I'm using
  * A [5" 40 pin TFT display](http://www.adafruit.com/products/1596) from
  Adafruit.
  * An [RA8875](http://www.adafruit.com/products/1590) display controller.
